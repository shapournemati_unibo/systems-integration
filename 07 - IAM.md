# IAM

Per IAM (Identity and Access Management) si intende il servizio di un sistema informatico che fornisce le seguenti **funzionalità**:

- authentication (o authN): verifica dell'identità di un utente del sistema;
- authorization (o authZ): autorizzazione di un utente all'utilizzo di un servizio;
- accounting: indicizzazione delle operazioni svolte dagli utenti.

Per via di tali funzionalità uno IAM è definito anche *servizio di AAA*.

Per ottemperare agli scopi descritti occorre che uno IAM integri altri servizi:

- **directory**: parte dello IAM che si occupa di indicizzare le informazioni su utenti, risorse, gruppi, ruoli e permessi in una base dati, e fornire ==authorization LDAP==. Visto il legame indissolubile tra i due servizi i due nomi sono di fatto utilizzati come sinonimi (ad esempio in ambiente Microsoft si parla soltanto di directory). Nella maggior parte dei casi un servizio di directory integra un servizio di **user provisioning/deprovisioning** che si occupa dell'aggiunta/rimozione degli utenti dal database, ed un servizio di **password management** che permette di gestire le loro password;
- **PKI**: utilizzo e verifica di certificati digitali e di chiavi crittografiche;
- **single sign-on**: autenticazione di un utente trasversale a più entità in una singola sessione di lavoro, ovvero senza dover fornire più volte le credenziali;
- **role management**: gestione dei ruoli delle entità;
- **auditing**: controllo sistematico e documentato delle violazioni.

In base alla complessità del sistema informatico a cui lo IAM si rivolge potrebbe fornire ulteriori servizi, quali:

- **federation**: autenticazione di un utente cross-IAM. Un insieme di IAM federati tra loro è chiamato **federazione**;
- **time syncronization**: fornire un orologio condiviso con l'ora esatta;
- **naming**: mappare nomi degli host nei loro indirizzi IP;
- **file sharing**: condivisione di drive o singoli file tra le entità gestite dallo IAM.

## Architettura del sistema informatico

Dal punto di vista dello IAM gli **attori** del sistema informatico sono:

- subject: entità che accedono ai servizi (solitamente sono gli utenti);
- service provider: servizio (o risorsa) del sistema informatico;
- domain controller: server che detiene i seguenti attori:
  - identity and access manager: entità che autentica un utente e autorizza l'utilizzo dei servizi, ovvero lo IAM stesso;
  - directory: servizio contenente tutte le informazioni sui subject, service provider, e relativi permessi e politiche di sicurezza.

L'organizzazione del sistema informatico, ovvero come le sue entità debbano essere raggruppate, deve adattarsi ai seguenti aspetti:

- struttura, collocazione geografica delle sedi;
- topologia pesata (quindi considerando le prestazioni) della rete del sistema informatico;
- informazioni specifiche ed organizzazione già in essere (a patto che sia razionale, ndr.) di subject e service provider: deve permettere di modellare le informazioni specifiche del tipo di entità, la struttura gerarchica, i ruoli, le responsabilità, i diritti sulle altre entità e le credenziali di autenticazione allo IAM.

Il primo strumento utile per organizzare tali entità è il **dominio**. Un dominio è un insieme di entità servite da uno stesso domain controller, che quindi condividono uno servizio di directory. Per quanto riguarda il servizio IAM per ogni dominio è possibile specificare un'unica procedura di autenticazione, mentre per il servizio di directory è utile, unitamente ad altre strutture organizzative, per gestire le politiche di autorizzazione. Se ne deduce quindi che il concetto di dominio non è pensato per modellare i confini geografici o altre caratteristiche fisiche del sistema informatico, ma occorre comunque considerarli in alcuni particolari casi (ad esempio in Microsoft Active Directory). I domini sono annidabili ed un dominio può essere gestito o dal suo domain controller, o da un domain controller di un dominio padre.

Un'altra struttura organizzativa utilizzata dal servizio di directory è l'**organizational unit (OU)**. Una OU è un gruppo contenuto nei confini di un dominio con cui modellarne i suoi gruppi, in modo da poter unificare l'applicazione di policy su più entità. Sono pensati per unire entità in base a criteri logici (esempio: il gruppo dei servizi web, il gruppo degli studenti, ...). Un'entità può appartenere a più OU. La OU non costituisce un sotto-domino del dominio che la contiene, quindi non permette di registrare più entità con lo stesso identificatore.

Solitamente uno IAM è pensato per fornire i suoi servizi a delle reti (di cui fa parte) con confini ben definiti ed amministrate da una entità well-known, risultando quindi una rete più sicura rispetto ad una il cui accesso non è controllato (come internet), caratteristica necessaria vista la sensibilità delle informazioni gestite dallo IAM. Vista però la necessità sempre più ingente di esporre i **servizi** anche ad utenti che si autenticano **attraverso reti esterne** a quelle di dominio gli IAM si sono adoperati di conseguenza, predisponendo tecnologie ad-hoc (ad esempio SAML). Si noti che comunque non tutti i servizi possono essere esposti per motivi di sicurezza, e quelli esposti devono comunque sottostare a politiche di sicurezza molto più rigorose; è consigliabile eseguirli in un dominio a se stante, considerato "di frontiera". Quindi ogni sistema informatico che fornisce sia servizi pubblici che locali avrà almeno due domini, uno pubblico con nome "organizzazione" ed uno locale con nome "organizzazione.local" (ad esempio per l'organizzazione UniBo saranno "unibo.it" ed "unibo.local").

Da un altro punto di vista è possibile affermare che il servizio IAM è un servizio **centralizzato**. Anche se la centralizzazione rende tale servizio un single point-of-failure di notevole importanza, visti i ruoli che ricopre, permette di:

- evitare di ridondare le informazioni su utenti e risorse, rendendone la gestione più agevole e diminuendo notevolmente la possibilità di configurazioni errate;
- evitare problemi dovuti alla mancanza di congruenza delle informazioni;
- implementare il servizio di single sign-on, altrimenti impossibile.

Risulta quindi molto importante che tale servizio sia **resiliente**, applicando i seguenti accorgimenti:

- ridondare le informazioni su più server, definendo comunque un ordine di preferenza per l'interrogazione;
- scalare il carico di lavoro tra più server in base alla mole di richieste che dovranno soddisfare;
- replicare i server in modo da ottimizzare i tempi di accesso.

Il **servizio di directory**, nella maggior parte dei casi, aderisce allo standard X.500, redatto dall'[ITU-T](https://it.wikipedia.org/wiki/ITU-T). Tale standard definisce che il servizio di directory sia formato da:

- un server, il server del servizio di directory, composto da:
  - un database;
  - un Directory System Agent (DSA): il DBMS del database descritto al punto precedente;
- vari Directory User Agent (DUA), ovvero la parte dei software client che interrogano il DSA;
- Lightweight Directory Access Protocol (LDAP), il protocollo di comunicazione tra DUA e DSA. LDAP è la versione su TCP di DAP, il protocollo inizialmente previsto dallo standard.

## Il directory database

Lo standard X.500 definisce le seguenti caratteristiche del database:

- ottimizzato in lettura, in quanto le query saranno in numero molto maggiore rispetto alle modifiche. Per questo motivo:
  - non supporta ne transazioni, ne rollback;
  - possibilità di definirne più repliche dislocate nel sistema informatico con consistenza lasca, ovvero possono essere consultate anche se non sono totalmente sincronizzate.
- architettura master-slave;
- possibilità di avere amministratori locali
- la struttura del database, chiamata **schema**, definisce la struttura degli oggetti che conterrà. Gli oggetti sono chiamati **entry** e la struttura di una singola entry è chiamata **objectclass** o classe.

Lo schema è organizzato in modo gerarchico, formando una struttura ad albero chiamata **Directory Information Tree (DIT)**, fisicamente memorizzata in file con struttura sequenziale, chiamata DIB. Lo schema sarà ottimizzato in ricerca, sempre perché il database è ottimizzato in lettura.

### ObjectClass ed Entry

Una objectclass è composta da:

- un OID come identificatore;
- un nome;
- degli attributi. Ogni attributo ha:
  - tipo:
    - identificato con OID
    - ha un nome
    - regole che regolano i valori che può contenere
    - regole per definire come confrontare i valori
    - può possedere delle opzioni

La radice di tutte le classi è la classe "top", che espone un unico attributo di nome "objectClass" che indica il tipo di classe.

I tipi di classi sono:

- abstract: non può essere instanziata direttamente;
- structural: può essere instanziata direttamente;
- auxiliary: classe che non può essere instanziata se non combinandola con una classe structural. È possibile quindi affermare che il directory schema supporta l'ereditarietà multipla.

Lo schema, le classi, gli attributi ed i tipi di attributo sono dichiarati con la **sintassi ASN1**.

La sintassi ASN1 fa uso degli **Object Identifier (OID)** come identificatori di tutte le entità che descrive. Gli OID sono degli identificatori formati da numeri separati da "." che identificano qualsiasi entità: dagli oggetti ObjectClass fino agli oggetti del mondo reale. Sono organizzati ad albero e qualunque azienda può richiedere allo IANA una sua foglia in tale albero, specificatamente una foglia del ramo 1.3.6.1.4.1 (che corrisponde a iso.org.dod.internet.private.enterpise) detta Private Enterprise Number (PEN), da estendere con le sue entità. In questo modo più aziende possono definire gli stessi oggetti senza che questi siano effettivamente uguali.

Visto quanto siano prolisse le descrizioni di classi ed attributi secondo la sintassi ASN1 è stato redatto uno standard che permette di rappresentare le stesse informazioni in una stringa più leggibile, il **LDAP Data Interchange Format (LDIF)**.

Per ogni classe si dichiara l'insieme dei **naming attributes**, ovvero gli attributi della classe compongono il **Relative Distinguished Name (RDN)** delle sue entry. Tale nome deve essere diverso da tutte le altre entry che nel DIT risiedono allo stesso livello. Il nome completo di una entry include anche gli RDN di tutte le entry padre (queste ultimeformano il **Base Domain Name (BDN)**), formando il **Distinguished Name (DN)**. L'insieme dei naming attributes di un DIT è detto **LDAP naming model**.

![Esempio di DIT.](images/Directory service - Esempio di DIT.svg){#fig:directory_service_esempio_dit}

Notare le sintassi e l'ordine dei valori di DN, RDN e BDN riportate in figura @fig:directory_service_esempio_dit, regolate secondo lo standard IETF RFC 4514. La rappresentazione è case-insensitive e prevede alcuni caratteri speciali, di cui occorre fare l'escape se devono essere utilizzati ocme caratteri nomali.

Gli standard X.500 ed RFC 2798 definiscono alcune **classi di default**, utili sopratutto nei primi livelli del DIT. Nei successivi livelli è possibile dichiarare oggetti custom, utili a soddisfare le necessità dell'organizzazione. Gli attributi di default definiti in queste classi sono:

| **Sigla** | **Attributo**     | **Descrizione** |
| --------- | ----------------- | --------------- |
| dc        | domain component  |                 |
| o         | organization      |                 |
| ou        | organization unit |                 |
| uid       | user id           |                 |
| cn        | common name       |                 |
| sn        | surname           |                 |
| l         | location          |                 |
| st        | state             |                 |
| c         | country           |                 |

Di seguito un'istanza di "inetOrgPerson" di esempio in formato LDIF:

```
dn: uid=jbrown,ou=People,dc=keycloak,dc=org
objectclass: top
objectclass: person
objectclass: organizationalPerson
objectclass: inetOrgPerson
uid: jbrown
cn: James
sn: Brown
mail: jbrown@keycloak.org
postalCode: 88441
userPassword: password
```

## Sicurezza

L'utilizzo di uno IAM permette di controllare e monitorare l'operato di un utente sia su ogni singola risorsa che sull'intero dominio.

Non risolve però i problemi derivanti dal furto delle credenziali, con il quale un'entità qualsiasi si può spacciare per un'altra ed intraprendere operazioni a lui non concesse o senza esserne l'effettivo responsabile. L'unica politica con cui uno IAM può mitigare le problematiche scaturite da questa casistica è comunicare via rete le credenziali di autenticazione di risorse ed utenti il meno possibile, ad esempio sostituendole a delle credenziali usa e getta, o richiedere la ri-autenticazione ogni tot ore o in determinati eventi. Su questo argomento la legislazione italiana prevede delle direttive.

Considerando che l'entità che detiene il servizio di directory ha pieni poteri sui dati che conserva, se si utilizzano sistemi di auth esterni occorre considerare che ci si sta implicitamente fidando di tale entità. Nulla vieta che questa possa smateriare e fare quello che vuole con questi dati; sicuramente successivamente sarebbe perseguita a livello legale, ma intanto i dati sono stati compromessi.

Esempio: se utilizzo il sistema di autenticazione Facebook sto affidando a Facebook i dati che lui richiede per creare la mia identità digitale. I dati che non richiede lui ma che richiedo io li posso hostare io, ma se ho premura che i dati hostati da Facebook non possano essere compromessi non devo utilizzare Facebook.

## La fase di Autenticazione

L'utilizzo di un sistema di IAM richiede che gli attori siano configurati in modo tale da utilizzarlo; se ne deduce che uno IAM non si interpone fisicamente tra gli attori ed il sistema, diversamente da quello che accade con altri sistemi (come i firewall, ad esempio), e che il rispetto delle politiche di sicurezza è sempre demandato alla logica che governa la singola risorsa (come il sistema operativo di un computer). Una volta configurato i dettagli dell'attore saranno memorizzati nella directory e sarà potenzialmente in grado accedere a tutte le altre risorse presenti nel dominio, oltre a poter instaurare una comunicazione criptata con lo IAM basando la criptazione anche sulle rispettive identità digitali. L'operazione di configurazione di un service provider all'utilizzo di uno IAM è detta **join**, mentre l'aggiunta di un subject è chiamata **accreditamento**.

Ogni volta che un subject desidera utilizzare una risorsa deve autenticarsi, ovvero identificarsi alla risorsa, in modo tale che questa possa controllarne e tracciarne l'operato in base ai diritti che gli sono stati concessi ed alle politiche di sicurezza. L'assegnazione di specifici diritti, o **granting**, avviene prima che il subject utilizzi le risorse. In molti sistemi il granting avviene secondo il cosiddetto "diritto di copia"; questo consente al possessore di un oggetto di concedere un qualsiasi diritto in suo possesso ad altri soggetti. Il possessore inoltre può anche concedersi oppure sottrarsi diritti. In altri sistemi invece i possessori non hanno né diritto di possesso né diritto di copia, i diritto sono tutti stabiliti dall'amministratore di sistema.

### Autenticazione all'utilizzo di LAN/MAN

L'autenticazione su canali trasmissivi della famiglia degli standard IEEE 802 (ethernet, WiFi, ...) è definita nello standard IEEE 802.1x e permette di autenticare un dispositivo tramite delle credenziali, diversamente da altri metodi che prevedono l'utilizzo di una password condivisa (WEP, WPA, ...).

Questo standard prevede i seguenti **attori**:

- supplicant: attore che desidera utilizzare il canale trasmissivo (ad esempio un dispositivo ethernet, un dispositivo WiFi, ...). Utilizza il protocollo EAP (Extensible Authentication Protocol, noto anche come EAPOL, EAP over LAN), che permette di negoziare diversi metodi di autenticazione. Solitamente si utilizzano EAP-TLS in congiunzione a certificati X.509 e PEAP in congiunzione a MS-CHAPv2 con username o password;
- authenticator: chiede l’autenticazione prima di offrire il servizio (ad esempio uno switch ethernet, un access point WiFi, ...) ;
- authentication server: verifica le credenziali del supplicant a nome dell’authenticator. Nello specifico un server che supporta i protocolli RADIUS o Diameter, successore di RADIUS.

Gli authenticator possono gestire l'autenticazione soltanto se prima hanno sono stati aggiunti all'authentication server, che avviene con lo scambio di una chiave segreta tra i due attori per criptare la loro connessione.

Tutti i protocolli utilizzati sono criptati.

![Schema generale degli attori e delle comunicazioni dello standard 802.1X. "EAP" in rosso indica un qualsiasi metodo di autenticazione.](images/802.1X - Schema generale.png){#fig:802.1X_schema_generale}

L'autenticazione avviene come segue:

1. il supplicant contatta l'autheticator tramite il protocollo EAPOL, fornendo le sue credenziali tramite messaggi definiti dal metodo di autenticazione utilizzato;
2. l’authenticator inoltra all'authenticator server il contenuto dei pacchetti EAPOL attraverso i protocolli RADIUS o Diameter;
3. l'authentication server controlla nei suoi registri la correttezza delle credenziali presentate e risponde di conseguenza.

Si evince quindi che l'authenticator ha un ruolo prettamente passivo.

Anche se non è previsto dallo standard alcuni authentication server permettono di definire uno IAM esterno da cui attingere le credenziali, così da unificare i due directory service. In caso un dominio sia composto da più sotto-domini geograficamente dislocati, come nel caso di UniBo ed i suoi campus, solitamente si fornisce uno IAM centralizzato ed un auhentication server per ogni sotto-dominio per mere questioni di performance: a confronto il protocollo RADIUS richiede lo scambio con l'authenticator server di un numero maggiore di messaggi rispetto a quelli previsti dai protocolli utilizzati dagli IAM.

In scenari in cui il supplicant cambia l'autenticator a cui si è connesso, lo standard prevede che il processo di autenticazione si ripeta. Esistono però alcune tecnologie proprietarie (ad esempio Cisco ne fornisce un'implementazione) in cui i vari authenticator si interrogano tra di loro quando un nuovo wireless terminal entra nel loro campo e, nel caso questi sia già autenticato, si passano la "responsabilità" di gestirne la comunicazione, informando il wireless terminal attraverso un pacchetto ARP.

### Autenticazione di un utente su dispositivi di dominio

L'autenticazione di un utente attraverso i dispositivi di un dominio prevede che tale dispositivo sia stato joinato al dominio. Questo perché quel dispositivo può veicolare informazioni di più utenti ed ha accesso al directory service, quindi occorre essere certi che il dispositivo non contenga software malevolo che permetta ad un utente di diventarne un altro.

Si potrebbe però rendere possibile la verifica delle credenziali utente, ovvero l'accesso ad directory service, anche ad entità del dominio non joinate; in tal caso si potrebbe comunque configurare l'autenticazione "di dominio" sul dispositivo senza che però il dispositivo ne abbia fatto il join. Viene da se che, in questo caso, la macchina non sarebbe tracciata e non è garantito l'accesso (potenziale) a tutte le altre risorse di dominio.

### Autenticazione di un utente sulle singole applicazioni

L'accesso di utenti ad un applicativo di dominio differisce in base al dispositivo dal quale si richiede l'accesso:

- da dispositivo joinato al dominio (al quale l'utente si è autenticato): in tal caso è già stato identificato l'utente, quindi l'applicazione sarebbe soltanto una risorsa messa a disposizione dal domino;
- da dispositivo non joinato al dominio: in tal caso la questione è delicata, perché il dispositivo non è well-known e potrebbe tentare di utilizzare il canale autenticato per attaccare il dominio. Occorre quindi autorizzare all'utilizzo soltanto ciò che è necessario ai fini dell'utilizzo dell'applicativo. 

### Servizio di SSO (Single Sign-On)

Per SSO si indica un meccanismo che concede ad un utente l'autorizzazione ad utilizzare più applicazione mediante una sola richiesta di credenziali.

L'implementazione del servizio di SSO deve tenere in considerazione di:

- dove si trova il computer su cui opera l'utente: nello stesso dominio in cui operano i server delle applicazioni oppure fuori da quel dominio;
- quale sistema operativo opera sul computer dell'utente;
- le applicazioni da utilizzare sono a base web o no: nel caso web infatti esistono vari meccanismi standard di sessione (cookie e altro) per mantenere le informazioni di autenticazione, come i token di accesso.

## Sofware

La tabella in @fig:IAM_comparativa_configurazioni_software compara le configurazioni software attualmente più comuni per l'implementazione di uno IAM:

![Configurazioni software attualmente più comuni per l'implementazione di uno IAM.](images/IAM - Comparativa configurazioni software.svg){#fig:IAM_comparativa_configurazioni_software}

Dalla tabella si nota l'esistenza di una buona varietà di software; ognuno di loro fornisce i servizi di authN, authZ ed auditing secondo diverse filosofie e sono studiati su sistemi di diversa dimensione e complessità, quindi richiedono anche un quantitativo di risorse differente. 

### Sistemi di sola autenticazione

#### NIS (Network Information Service)

NIS è un software per sistemi UNIX che permette la condivisione dei contenuti di alcuni database di sistema utili all'implementazione di alcuni servizi IAM. In Linux tale software è stato integrato nel kernel.

Nello specifico i servizi realizzati e relativi file condivisi sono:

- servizio di directory: 
  - "/etc/passwd": informazioni sugli utenti;
  - "/etc/group": informazioni sui gruppi;
  - "/etc/shadow": shadow password (password criptate);
- servizio di naming:
  - "/etc/hosts": traduzione degli indirizzi IP degli host della rete locali;
  - "/etc/networks": traduzione degli indirizzi IP delle sottoreti (locali);
  - "/etc/protocols": nomi e numeri dei protocolli di rete;
  - "/etc/aliases": alias di posta elettronica;
- altri servizi:
  - "/etc/rpc": numeri delle chiamate RPC;
  - "/etc/services": abbinamento dei servizi di rete ai numeri di porta corrispondenti.

Gli **attori** di questo sistema sono:

- server master: possiede la copia autoritativa (= principale) dei database, e risponde alle richieste di lettura e modifica provenienti dai client;
- client: le macchine da cui un utente può accedere;
- server slave: sono dei client che mantengono una copia non autoritativa del database del server master, utile in caso il server master non sia più raggiungibile.

Il dominio delle macchine con cui i database sono condivisi è chiamato *NISdomain*.

Tutte le operazioni dei client avvengono direttamente sui database del server master tramite il protocollo RPC (Remote Procedure Call). I server slave aggiornano la loro copia dei database su loro iniziativa o in caso il server master li notifichi dell'avvenimento della modifica.

Per via della logica "artigianale" di NIS e dell'utilizzo di RPC, protocollo oramai caduto in disuso in quanto rappresentava una potenziale backdoor, se ne consiglia l'utilizzo soltanto per micro-domini.

Ulteriori informazioni su NIS ed istruzioni di configurazione:

- https://www.linux.com/news/introduction-nis-network-information-service;
- https://www.freebsd.org/doc/handbook/network-nis.html;
- http://www.archive.org/details/AppuntiDiInformaticaLibera.

#### PAM (Pluggable Authentication Modules)

PAM è un meccanismo per utilizzare un servizio di autenticazione locale alla macchina in una qualsiasi applicazione senza che questa ne conosca lo schema, ovvero permettendo al software di utilizzare un'autenticazione senza che sappia come questa avvenga. Disponibile per la maggior parte dei sistemi UNIX è implementato su s.o. Linux dal progetto [Linux-PAM](http://www.linux-pam.org/).

PAM permette di specificare lo schema di autenticazione in base all'applicazione che la richiederà. Nella cartella "/etc/pam.d" infatti risiederanno più file, definiti dall'amministratore di dominio e nominati come l'applicazione che dovrà utilizzarlo; in questo modo **le modalità di autenticazione saranno gestite dall'amministratore di sistema** e, con opportune metodologie, condivise tra tutte le macchine di dominio e quindi gestite dall'organizzazione. Si deduce inoltre che questo sistema può essere utilizzato anche da macchine non joinati al dominio se il directory service è "pubblicamente" raggiungibile.

L'autenticazione avviene come segue:

1. l'applicazione richiama la routine `pam_authenticate`;
2. PAM esegue le operazioni specificate nel file di configurazione specifico per l'applicazione "/etc/pam.d/NOME_APPLICAZIONE";
3. il risultato dell'autenticazione sarà restituito all'applicazione.

![Schema generale di PAM.](images/PAM - Schema generale.jpg)

##### Configurazione

- The Linux-PAM System Administrators' Guide: http://www.linux-pam.org/Linux-PAM-html/Linux-PAM_SAG.html
- Moduli PAM standard: http://www.linux-pam.org/Linux-PAM-html/sag-module-reference.html
- Come scrivere dei propri moduli PAM: http://www.linux-pam.org/Linux-PAM-html/Linux-PAM_MWG.html

#### SAML 2.0 con Shibboleth

 SAML (Security Assertion Markup Language) è uno standard per realizzare un servizio di SSO per sole applicazioni web indipendente dall'accesso da dentro o fuori dominio grazie all'utilizzo dei cookie per identificare l'utente (quindi la sessione di lavoro è unica per istanza di browser). Si tratta di uno standard arrivato alla versione 2.0, che è sostanzialmente differente dalla versione 1.0, e non è mantenuto da nessun ente di preciso, ma viene dal mondo aziendale e quindi potrebbe essere rimpiazzato da una soluzione concorrente nel caso questa ne scardini l'egemonia. L'implementazione che tratteremo è Shibboleth.

Lo standard SAML prevede che l'autenticazione e l'autorizzazione dell'utente all'applicazione web non sia gestita dal service provider, ma dallo IAM. Questo è il punto di forza di questa tecnologia, che così facendo:

- aumenta la sicurezza del sistema, visto che solitamente gli applicativi web di un dominio sono esposti anche al di fuori del dominio;
- non necessitare, e quindi poter non permettere, l'accesso delle informazioni contenute nello IAM all'applicazione web;
- salvando determinate informazioni nei cookie del browser client permette di evitare all'utente il reinserimento delle credenziali in caso sia nuovamente necessaria l'autenticazione.

La procedura di login è la seguente:

1. il client, che non ha precedentemente effettuato nessun login SAML presso lo IAM, contatta un' applicazione web, ovvero un service provider;
2. l'applicazione web redireziona il client alla pagina di login dello IAM;
3. l'utente fornisce le credenziali e lo IAM verifica che siano corrette e che gli sia permesso l'utilizzo dell'applicativo web;
4. in caso positivo lo IAM redireziona il client alla pagina dell'applicativo web, aggiungendo alla richiesta di redirect un token temporaneo (in GET o POST in base alle scelte dell'amministratore di sistema). Tale token permette di risalire all'utente per il quale è stato creato;
5. l'applicativo web richiede allo IAM conferma del token ricevute e, in caso positivo, all'utente è concesso l'accesso all'applicativo.

Il logout prevede che dopo la richiesta da parte dell'utente di logout, lo IAM informi tutti gli applicativi a cui aveva richiesto un'autenticazione dell'avvenuto scollegamento.

#### Kerberos

Kerberos è un sistema di autenticazione nato nel 1983 in collaborazione tra M.I.T., IBM e DEC, ed attualmente incluso nei software IAM di una certa consistenza, come Microsoft Active Directory e Samba. Il suo nome deriva da Cerbero, il cane a tre teste della mitologia greca a guardia dell'inferno, di cui ogni testa identifica uno degli scopi per il quale è nato:

- autenticazione: verificare l'identità di un attore;
- identificazione: identificare un subject che contatta un service provider, cosicché quest'ultimo possa concedere o meno l'accesso al subject;
- riservatezza: prevenire che terze parti possano intercettare i contenuti di qualunque comunicazione.

Dati questi scopi si deduce come Kerberos sia stato progettato per essere utilizzato su reti pubbliche, ma per via di dettagli implementativi non sarebbe sicuro, quindi è utilizzato soltanto internamente un dominio. Comunque sia Kerberos implementa anche il SSO.

##### Architettura e funzionamento

Dal punto di vista di Kerberos un dominio presenta i seguenti **attori**:

- il KDC (Key Distribution Center): è la terza parte del modello di fiducia alla base di Kerberos. Composto da:
  - AS (Authentication Service): servizio che autentica l'utente al KDC;
  - TGS (Ticket Granting Service): servizio che rilascia i token di autenticazione che il client utilizza per identificarsi al service provider;
  - Kerberos Database: database che detiene le chiavi di identificazione e gli identificatori dei principal del realm (vedi sotto);
- i principal: sono i subject od i service provider. Ciascun principal è identificato da nome nel formato "\<primary\>/\<instance\>@\<realm\>", ove:
  - \<primary\>: nome del principal;
  - \<instance\>: qualifica del principal.

I principal sono organizzati in **realm** (= reame), quindi l'accreditamento di un principal ad un realm significa configurare un'istanza di Kerberos in modo tale che i principal si possano autenticare tramite questa. Kerberos è in grado, se configurato, di redirezionare le richieste da principal di realm differenti all'istanza di Kerberos interrogata; è il caso del single-sign-on proposto in vari siti che permettono il login attraverso altre autorities, come Facebook o OpenID.

Un principal però può essere accreditato soltanto se implementa il protocollo fornito da Kerberos, ovvero se è **kerberized**; vista la potenza e diffusione di Kerberos molte applicazioni di una certa entità supportano il protocollo out-of-the-box.

L'autenticazione tramite Kerberos avviene a *chiave simmetrica*; infatti ciascun principal condivide con il KDC una chiave crittografica. Per i subject che accedono con nome utente e password, ovvero gli utenti umani, la chiave è dedotta da loro password, che quindi deve essere di complessità non banale per fornire un buon livello di sicurezza come specificato dai requisiti di Kerberos, mentre per gli altri subject (difficile che esistano) e per le risorse occorre aggiungere manualmente la loro chiave al KDC, visto che il protocollo non fornisce un sistema sicuro di scambio della chiavi (anche perché non esiste).

Le specifiche di Kerberos inoltre indicano che il KDC di ogni realm deve essere eseguito in una macchina fisica sicura e fidata per via del fatto che l'implementazione del KDC richiede di esporre delle porte di rete. Queste porte costituiscono un minimo punto di attacco e per questo motivo *non è buona norma fornire un servizo Kerberos su una rete pubblica*, ma è perfetto per reti di dominio anche di grandi dimensioni.

![Fasi dell'autenticazione su di una risorsa (un server Tomcat) attraverso Kerberos. Nell'immagine è riportata per completezza anche la fase di autorizzazione.](images/Kerberos - Autenticazione.svg){#fig:kerberos_autenticazione}

Facendo riferimento alla @fig:kerberos_autenticazione si descrive il protocollo di autenticazione di un utente con username e password presso una risorsa kerberizzata, estensione del modelli di distribuzione delle chiavi di Needham e Schroeder:

0. l'utente fornisce al client della risorsa, entrambi kerberizzati, le proprie credenziali testuali. Il client deduce la chiave di identificazione dell'utente $K_{User}$;

1. il client contatta l'AS del KDC. Il messaggio è così composto:

   $M_{1} = [options, ID_{User}, REALM_{User}, K_{TGS}, E_{K_{User}}(TIMES_{1}), NONCE_{1}]$

2. se il KDC riesce a decrittare $E_{K_{User}}(TIMES_{1})$ l'utente è autenticato. La chiave $K_{User}$ necessaria alla decriptazione è già presente nel suo database, essendo stata aggiunta nella precedente fase di accreditamento dell'utente.

   Se il tempo $TIMES_{1}$ non è ancora scaduto l'AS conferma l'autenticazione e genera un ticket con cui il client potrà autenticarsi presso il TGS, detto TGT (TicketGranting Ticket). Il messaggio di risposta è così composto:

   $M_{2} = [REALM_{User}, ID_{User}, TGT, E_{K_{User}}(K_{User,TGS}, ID_{TGS}, REALM_{TGS}, TIMES_{2}, NONCE_{1})]$

   $TGT = E_{K_{TGS}}(flags, K_{User,TGS}, REALM_{User}, ID_{User}, ADDRESS_{Client}, TIMES_{2})$

   $K_{User, TGS}$ è generata dal TGS e sarà utilizzata per criptare le successive comunicazioni tra User e TGS.

   $TGT$ potrà essere utilizzato per ottenere dal TGS un ticket di accesso a qualsiasi risorsa dello stesso realm, ovvero $REALM_{User}$, per tutto il tempo $TIMES_{2}$. In quel lasso di tempo quindi non occorrerà che l'utente si autentichi nuovamente all'AS, *ottenendo la dinamica del single-sign-on*.

   Il client salva $TGT$ nella Kerberos Tray dell'utente, area di memoria che deve risiedere in una memoria volatile e non può essere swappata.

3. il client richiede al TGS un ticket di accesso alla risorsa (o granting ticket) $GT$. Il messaggio di richiesta è così composto:

   $M_{3} = [options, ID_{resource}, TIMES_{2}, NONCE_{2}, TGT, AUTHENTICATOR_{User}]$

   $AUTHENTICATOR_{User} = E_{K_{User,TGS}}(ID_{User}, REALM_{User}, TIMES_{3})$ ha il compito di assicurare al TGS che l'utente che richiede il GT è lo stesso del TGT. Tale token sarà valido per $TIMES_{3}$.

4. se al TGS, confermata l'identità dell'utente, risulta che questi possa accedere alla risorsa richiesta, genera il GT. Il messaggio di risposta sarà composto da:

   $M_{4} = [REALM_{User}, ID_{User}, GT_{Resource}, E_{K_{User,TGS}}(K_{User,Resource}, ID_{Resource}, REALM_{Resource} TIMES_{2}, NONCE_{2})]$

   $GT_{Resource} = E_{K_{Resource}}(K_{User, Resource}, REALM_{User}, ID_{User}, ADDRESS_{Client}, TIMES_{3})$ (sarà valido per $TIMES_{3}$)

5. il client richiede accesso alla risorsa con $GT_{Resource}$. Il messaggio è così composto:

   $M_{5} = [options, GT_{Resource}, AUTHENTICATOR2_{User}]$

   $AUTHENTICATOR2_{User} = E_{K_{User,GT_{Resource}}}(ID_{User}, REALM_{User}, TIMES_{4}, SUBKEY, SEQ\#)$ ha il compito di assicurare alla risorsa che l'utente che richiede l'accesso ==è quello giusto==. Tale token sarà valido per $TIMES_{4}$.

6. la risorsa, appurata l'identità dell'utente, concede l'accesso. La risposta sarà così composta:

   $M_{6} = E_{K_{User,Resource}}(TIMES_{4}, SUBKEY, SEQ\#)$ (l'accesso sarà permesso per $TIMES_{4}$)

   Una volta scaduto l'accesso alla risorsa, l'utente dovrà richiederlo nuovamente. Se è scaduto soltanto $TIMES_{4}$ occorre ripetere la procedura dal punto 5, altrimenti in caso sia scaduto $TIMES_3$ occorre richiedere un nuovo $GT_{Resource}$, quindi ripetere la procedura dal punto 3.

7. la fase di autenticazione, quindi il compito di Kerberos, termina qui. Ora l'utente, attraverso il client, può utilizzare la risorsa secondo i permessi a lui concessi e controllati nelle varie fasi di autorizzazione successive.

==Un esempio di client può essere il pannello di login del sistema, che sarebbe il client di un servizio IAM kerberizzato - Per l'accesso ad altre risorse la Kerberos Tray sarebbe condivisa?==

> **Legenda**
>
> - $TIMES_{n}[TIMESTAMP_{send}, TTL_{M_{1}}]$:  indica per quanto tempo un messaggio sarà valido, con lo scopo di evitarne utilizzi illeciti da altre entità. Può prevedere anche un terzo campo $RENEWTIME$ per richiedere il rinnovo del lasso temporale;
> - $E_{K}(messaggio)$: messaggio criptato con un algoritmo di crittografia simmetrica utilizzando la chiave $K$;
> - $NONCE_{n}$: valore casuale per rendere più difficile attacchi volti a decriptare il messaggio.

Dalle dinamiche di autenticazione si può carpire il modello di fiducia implementato da Kerberos, chiamato *modello di fiducia a terza parte fidata*: questo modello di fiducia non richiede che gli attori di una comunicazione si fidino reciprocamente l'uno dell'altro, come nel sistema di firma digitale, ma si fidano tutti di una stessa terza parte il cui compito è quello di identificarli. Dal momento che sono tutti identificati da una terza parte fidata possono indirettamente fidarsi tra di loro.

Si nota l'importanza della sincronizzazione degli orologi degli attori; in caso di differenza tra gli attori accomunati da uno stesso ticket maggiore della durata del ticket, questo risulterebbe scaduto immediatamente dopo la generazione o generato nel futuro, quindi non valido.

Si può vedere inoltre come Kerberos scambi le chiavi detenute dai singoli, ovvero le loro chiavi segrete, soltanto criptandole con altre chiavi "usa e getta" e come non sia mai scambiata la password dell'utente, nemmeno criptandola.

Su reti TCP/IP il protocollo Kerberos utilizza come **UDP** come protocollo di livello di trasporto e la **porta 88** come porta per il servizio KDC, mentre la 1024 per il principal che richiede di autenticarsi.

##### Sicurezza [non studiare]

Kerberos è sensibile ai seguenti attacchi:

- attacco di tipo spoofing: durante lo scambio dei ticket è possibile per un pirata che ha totale controllo della rete intromettersi facendo credere a Kerberos di essere un client, avendo il totale controllo della rete. Ma come avviene?

  1. pirata tramite Backdoor,trojan prende possesso di un pc;
  2. lancia uno sniffer invisibile;
  3. intercetta un ticket,preleva le credenziali e si spaccia per il client al quale il ticket era riferito.
2. attacco brute force: nella risposta di Kerberos ad un principal c'è la possibilità di intercettazione della chiave privata rilasciata;
3. attacchi DDOS: nella versione 5 sono presenti due falle:
  - falla tipo "double-free": potrebbe essere sfruttata da un cracker per compromettere l'intera infrastruttura di autenticazione di una rete;
  - falla tipo "buffer overflow": potrebbe essere utilizzata da un aggressore sia per mandare in crash il KDC, sia per eseguire del codice.
- attacco alle credenziali di amministratore Kerberos: un pirata scopre la password dell’amministratore principale con la quale si ha completo
  accesso al database di kerberos;
- DOS: creazione carico elevato;
- The Insider: Kerberos non è protetto da possibili modifiche dannose ai privilegi da parte di utenti che possono accedere fisicamente al KDC;
- Social Engineering and Password Exposure:non è protetto da possibili divulgazioni non autorizzate di password o impostazioni di sicurezza;
- Off-line Password Attacks: Kerberos is vulnerable to this kind of attacks since a message is encrypted with a key derived from the client's password. The authenticator limits this issue because limits the number of AS responses.

Ovviamente possono esistere altre falle nel codice, creare un software assolutamente sicuro ad ogni release non è una banalità.

##### Kerberizzare applicazioni

"Kerberizzare" un'applicazione significa renderla in grado di utilizzare il protocollo Kerberos, personalizzazione che deve tenere conto anche se l'applicazione è un service provider od un client.

Le API di Kerberos v5 sono piuttosto complicate da utilizzare. Lo standard Generic Security Service Application Program Interface (GSSAPI o GSS-API), rilasciato da IETF nel 2000, definisce delle API che permettono di utilizzare vari servizi di sicurezza, tra cui Kerberos, ma purtroppo anche loro sono complicate. Per questo è stato definito lo standard Simple Authentication and Security Layer (SASL), esposto nella RFC 4422 del 2006, che permette di usare diversi meccanismi di sicurezza, tra cui quelli GSSAPI, quindi anche Kerberos.

### Sistema di autorizzazione - LDAP

LDAP è un protocollo su TCP per l'interrogazione di servizi di directory. 

### Microsoft Active Directory

Microsoft Active Director è l'implementazione di Microsoft di un servizio di directory, utilizzato a partire da Windows 2000 server. Essendo studiato per sistemi informatici aziendali di media e grande dimensione questo servizio non è soltanto di directory, ma è uno IAM a tutto tondo.

I protocolli che utilizza sono: Kerberos, LDAP, DNS ed NTP. DNS ed NTP sono molto utilizzate da Microsoft AD, quindi occorre prestare notevole attenzione alla loro configurazione. Kerberos e l'operazione "bind" di LDAP sono eseguiti su connessione TLS, cosa non prevista dai rispettivi standard ma implementata come sicurezza aggiuntiva.

#### Architettura del sistema informatico

Un sistema informatico basato su AD è diviso in:

- domini: insieme che raggruppa identità di subject, service providers e policy di sicurezza che li regolano;
- alberi: insieme di domini di una stessa foresta che condividono parte del nome. La radice di un albero è chiamata **root domain**;
- foreste: insieme di alberi che condividono lo stesso directory schema, global catalog e configurazione. Le radici degli alberi della foresta operano indipendentemente. La foresta è identificata dal uno dei domini radice degli alberi che la compongono, chiamato **forest root domain**. ==utili per comunicare domini divisi che sono gestiti dalla stessa enterpise==.

Il **global catalog (GC)** è un database che risiede nei DC di un dominio. Contiene tutte le informazioni delle entità del proprio dominio ed una versione ridotta (ovvero senza tutti gli attributi) delle entry degli altri directory database della foresta; lo scopo dell'indice sulle entry del resto della foresta è quello di rendere più veloci le operazioni relative alle entità esterne al domino. Più DC di un unico dominio possono contenere il GC, sia diviso o duplicato in toto; attenzione però che troppi GC creano un traffico di sincronizzazione della madonna.

Tra i domini è possibile instaurare dei **trust**: tali relazioni permettono ai subject di un dominio di accedere alle risorse di un altro dominio. I trust possono essere transitivi o intransitivi: i trust transitivi possono essere utilizzati sia dal domino che ha il trust, sia dai domini che hanno il trust su di esso. Si possono definire anche trust ridondanti, utili per velocizzare il resolve di un trust nel caso la catena di trust transitivi sia lunga. Di default sono istanziati trust bidirezionali tra i domini padre e quelli figlio di un albero o tra le radici degli alberi di una foresta, mentre possono essere aggiunti manualmente trust unidirezionali o bidirezionali tra altri domini.

![Esempio di foresta e relative relazioni di trust automaticamente istanziate.](images/AD - Trusts.png)

Ogni dominio può avere al massimo un **domain controller** o essere gestito dal domain controller di uno dei domini padre. Un domain controller può essere però messo in opera utilizzando più server che avranno un ruolo paritario, così da poter dividere il carico di lavoro ed avere una struttura più robusta; si consigliano almeno 2 server attivi più eventuale un server accessorio meno potente. Si consiglia di utilizzare IP fissi per i domain controller.

Le uniche funzionalità che non possono essere replicate su più domain controller server, per via del compito svolto da tali ruoli, sono i **ruoli Flexible Single Master Operation (FSMO)**. Tali ruoli possono essere trasferiti in qualunque momento da un DC all'altro, motivo per il quale sono definiti "flexible". I ruoli FSMO sono:

- Per-domains roles:
  - PDC Emulator - Importante per i PC pre-Windows 2000, è il master per la sincronizzazione degli orologi, gestisce i cambi di password degli utenti del dominio;
  - RID Master – Gestisce i Relative ID degli oggetti creati ed è responsabile dello spostamento di un oggetto da un dominio ad un altro;
  - Infrastructure Master – Si preoccupa che il riferimento di oggetti fra domini sia consistente
- Per-forest roles:
  - Schema Master – Gestisce i cambiamento allo schema della foresta e la propagazione delle modifiche agli altri DC
  - Domain Naming Master - Questo ruolo si occupa dell’aggiunta o rimozione di domini da una foresta.

In AD le **OU** hanno le seguenti caratteristiche:

- ne esistono dei tipi predefiniti, ma possono esserne anche definite di altre tipologie;
- possono essere dichiarate come "universal" e contenere oggetti anche di altri domini, pur rimanendo sempre nei confini del dominio proprietario.

Un altro costrutto di organizzazione per le sole risorse sono i **siti**, gruppi logici definiti su uno spazio di indirizzi IP indipendenti dalla definizione di domini ed OU. Sono utilizzati per indicare ad AD i costi dei collegamenti tra le varie sottoreti, cosicché possa regolare la frequenza delle operazioni di sincronizzazione per evitare sovraccarichi della rete, e per indicare alle risorse il DC più vicino. Si consiglia inoltre che esista almeno un DC con GC per ogni sito, per diminuire il traffico sulle tratte più costose.

Infine si noti che è possibile nominare diversi **amministratori** delegando determinati diritti di amministrazione a subject di dominio, sia per un dominio, che per un sito, che per una OU.

#### Organizzazione del dominio AD

Visto che AD utilizza per molti scopi il servizio DNS è buona prassi che la divisione in zone del servizio DNS sia considerata nell'organizzazione del sistema informatico. Una organizzazione ottimale potrebbe essere:

- ==definire dominio pubblico e local: utilizzeranno server DNS diversi, cosìcché gli indirizzi gestiti dal DNS local non saranno visibli dal dominio pubblico. viceversa invece? essendo separati no, però potrebbe essere utile==
- si definiscono dei domini con confini geografici per la gestione dei dispositivi fisici. per ognuno di tali domini, o almeno per ognuno dei domini fisicamente più distanti dagli altri, vi sarà un server DNS e domain controller in loco alla rete fisica corrispondente, sempre per ottimizzare l'accesso.
- ==definire sottodomini==
- ==per i subject? non si guarda la roba==
- ==per i service provider?==

==ogni dominio deve fare capo ad un DNS==

Sempre per motivi di load-balancing Microsoft AD permette di dividere l'albero del directory service di un dominio in più parti, ognuna assegnata ad uno dei domain controller server di dominio.

==La definizione dello schema è una delle cose più complicate dell'universo informatico, perché a seconda di come lo definisci condizioni organizzazione aziendale. Quando costruisci un servizio di directory, a meno che tu non sei in una organizzione micro, lo fai fare da uno di Microsoft che fa questo per lavoro.==

### Windows workgroups: NT LAN Manager (NTLM)

Suite di protocolli di sicurezza Microsoft che forniscono autenticazione, integrità e confidenzialità per gli utenti di un unico gruppo, chiamato **workgroup**. Funziona in modalità peer-to-peer.

Si utilizza ancora in situazioni particolari:

- il client si sta autenticando ad un server utilizzando un indirizzo IP;
- il client si sta autenticando ad un server che appartiene ad una foresta Active Directory differente che ha un legacy trust NTLM invece di un trust inter-foresta;
- il client si sta autenticando ad un server che non appartiene ad un dominio;
- non esiste un dominio Active Directory (ovvero quando esiste un workgroup oppure una rete peer-to-peer)
