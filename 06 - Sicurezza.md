# Sicurezza nei sistemi informatici

La sicurezza informatica è l'insieme dei mezzi e delle tecnologie tesi alla protezione dei sistemi informatici garantendo determinate proprietà di informazioni o risorse, quali:

- **confidenzialità** (o segretezza, riservatezza): protezione da accessi non autorizzati;
- **integrità**: difesa da alterazioni non autorizzate. Per le informazioni l'integrità si estende anche all'autore, ovvero occorrei garantire che l'autore dell'informazione non possa essere travisato;
- **non repudiabilità** (o rintracciabilità): associare un'informazione o un'azione su una risorsa ad un autore;
- **disponibilità**: accesso all'informazione o risorsa in tempi e modi ragionevoli.

Per garantire tali proprietà un sistema informatico mette in atto delle **politiche di sicurezza**, ovvero delle regole che specificano le operazioni che attori del sistema possono intraprendere ed i relativi diritti di esecuzione. Tali politiche saranno attuate attraverso dei **meccanismi di sicurezza** che saranno integrati nel sistema informatico in ogni punto utile - non esiste un livello architetturale preposto ai meccanismi sicurezza, sono logiche che permeano in più livelli.

## Meccanismi di sicurezza

I meccanismi di sicurezza sono divisi in meccanismi di:

- **prevenzione**: meccanismo che impedisce un'operazione non consentita. Solitamente sono pesanti ed interferiscono con il modo in cui l'attore utilizza un'informazione od una risorsa. Esempio: richiesta di una password;
- **scoperta** (o auditing): meccanismo che permette di rilevare azioni considerate pericolose, sia passate o che stanno avvenendo al momento. Utile quando non è possibile prevenire, come nei casi di operazioni non prevenibili per via di limiti del sistema di prevenzione ma comunque vietate, o per valutare le misure di prevenzione messe in atto;
- **recupero**: meccanismo di ripristino del sistema durante o dopo un'operazione non consentita.

Per essere efficace un meccanismo deve essere un giusto trade-off tra:

- livello di alfabetizzazione informatica dell'utente, requisiti a cui dovrà ottemperare e comportamenti che dovrà attuare;
- peso computazionale del meccanismo da adottare;
- tempi e costi di implementazione e manutenzione;
- protezione sufficiente dai pericoli a cui fronteggia;
- ottemperare agli obblighi di legge, se presenti.

### Meccanismi di prevenzione

#### Crittografia

La crittografia è una branca della scienza che studia il modo di codificare un testo in chiaro, ovvero leggibile, in un testo codificato, ovvero non leggibile, e viceversa.

Le tecniche di **crittografia con chiave** (o seed) sono considerate le più sicure perché per decriptare un messaggio, oltre a sapere l'algoritmo utilizzato, richiedono di essere in possesso della necessaria chiave. Per questo tipo di tecniche di criptazione la dimensione della chiave è inversamente proporzionale alla probabilità che con un attacco brute-force con le attuali potenze computazionali.

##### Crittografia a chiave simmetrica

La tecnica di criptazione a chiave simmetrica (o segreta) utilizza una unica chiave sia per criptare che decriptare; occorre quindi condividere la chiave tra tutti gli interlocutori che vogliono instaurare una comunicazione confidenziale.

Le criticità di questa tecnica sono:

- la segretezza del contenuto criptato dipende dalla sicurezza con cui gli interlocutori conservano la loro copia della chiave;
- non esiste un metodo assolutamente sicuro per scambiare una chiave che, per definizione, deve rimanere segreta;

Dal punto di vista della sicurezza questa tecnica garantisce la confidenzialità della comunicazione e l'autenticazione del mittente in caso di comunicazione tra due interlocutori, ma non garantisce la proprietà di non repudiabilità del mittente verso entità esterne alla comunicazione visto che la chiave di criptazione è la stessa per tutti gli interlocutori.

Gli algoritmi che implementano questa tecnica sono computazionalmente leggeri e forniscono una buona sicurezza con chiavi relativamente piccole: ad esempio Triple-DES ed AES (Advanced Encription Standard) utilizzando chiavi da 64,128,168 e 256 bit.

##### Crittografia a chiave asimmetrica

La tecnica di criptazione a chiave asimmetrica (o pubblica) si basa sul calcolo ed utilizzo di due chiavi tali che un tasto criptato con una può essere decriptato soltanto con l'altra e viceversa. Ogni interlocutore deve generare una coppia di chiavi, formata da:

- chiave privata: chiave personale dell'interlocutore. Lo identifica, quindi non deve essere redistribuita;
- chiave pubblica: chiave complementare a quella privata di dominio pubblico.

La tecnica impone inoltre che dalla chiave pubblica non possa essere dedotta quella privata, mentre il contrario è possibile.

La comunicazione garantisce diverse caratteristiche di sicurezza in base al modo in cui si utilizzano le chiavi:

- contenuto criptato con la chiave pubblica: sarà decriptabile soltanto con la chiave privata. Si instaura quindi una comunicazione confidenziale tra l'interlocutore che cripta e l'interlocutore detentore della chiave privata, ma non è garantita l'autenticazione (quindi non repudiabilità) del mittente visto che per criptare si è utilizzata la chiave pubblica;

- contenuto criptato con la chiave privata: sarà decriptabile soltanto con la chiave pubblica. La comunicazione non è confidenziale, visto che chiunque può decriptare il messaggio, ma è garantita l'autenticità (quindi la non repudiabilità) del mittente.

- contenuto criptato con la chiave criptata del mittente e la pubblica del destinatario: è l'unione dei due casi casi precedenti, la comunicazione fornirà tra due interlocutori sia l'autenticazione del mittente che la confidenzialità con il destinatario.

  ![Crittografia asimmetrica "doppia".](images/Crittografia - Asimmetrica doppia.svg)

Gli algoritmi che implementano questa tecnica sono computazionalmente più pesanti di quelli a chiavi simmetrica ed utilizzano chiavi più grandi: ad esempio RSA (dagli autori Rivest, Shamir ed Adelson), l'algoritmo a chiave asimmetrica più utilizzato, fornisce un buon livello di sicurezza con chiavi di dimensione maggiore ai 2048 bit.

Va comunque notato che questa tecnica di criptazione non fornisce un metodo per assicurarsi dell'identità dell'interlocutore associato ad una chiave pubblica.

#### Hashing crittografico

Le funzioni di hashing crittografiche (o funzioni di one-way hash o funzioni di digest) sono delle funzioni che dato un testo, a lunghezza variabile, producono un testo a lunghezza fissa chiamato *digest* che possiamo considerare come riassunto del messaggio. Differiscono dagli algoritmi di criptazione perché il risultato non è reversibile, ovvero dato il digest non è possibile risalire al messaggio da cui è stato generato, e differiscono dalle funzioni di checksum perché ==non forniscono una certa proprietà che non ho quale sia, il testo dice "la 1 e la 2 si, ma non la 3"==.

Gli algoritmi di hashing sono computazionalmente più leggeri degli algoritmi di criptazione. I più noti sono:

- MD5 (Message Disgest 5): vecchiotto e non più considerato sicura, genera un digest da 128 bit;
- algoritmi della famiglia SHA (Secure Hash Algoritm):
  - SHA-1: considerato poco sicuro, genera un digest da 160bit;
  - SHA-2: indica tutte le versioni successive, che differiscono per il numero di bit dell'hash, nello specifico SHA-224, SHA-256, SHA-384, SHA-512. Ovviamente affidabilità e costo computazionale crescono utilizzando hash di dimensione maggiore.

#### MAC e firma digitale

Le tecniche MAC e firma digitale permettono di garantire l'integrità di un messaggio e l'autenticità dell'interlocutore, senza però criptare l'intero messaggio, che risulterebbe computazionalmente oneroso vista la grande dimensione dei messaggi scambiati al giorno d'oggi.

Le due tecniche differiscono soltanto per l'utilizzo di una chiave simmetrica nel caso della MAC e di una coppia di chiavi asimmetriche nel caso della firma digitale, ma il funzionamento è lo stesso:

- alla spedizione il messaggio è corredato con il suo digest crittografato, chiamato anche MAC;
- alla ricezione il destinatario può assicurarsi che il mittente sia quello desiderato verificando che la decriptazione del digest vada a buon fine, ed assicurarsi che il messaggio non sia stato alterato confrontando il digest calcolato con quello ottenuto dalla decriptazione.

La sottile differenza tra le due tecniche è la stessa che si presenta nelle tecniche di criptazione, ovvero la tecnica MAC, che utilizza la criptazione simmetrica, non permette di garantire la non repudiabilità del mittente verso entità esterne alla comunicazione.

### Meccanismi di auditing

Certe "garanzie" possono non bastare:

- anche azioni autorizzate possono causare violazioni di sicurezza
- ci può essere un "buco" nei controlli che abbiamo stabilito
- impossibile la sicurezza al 100%, alcune violazioni di sicurezza potrebbero comunque accadere.

Occorre verificare, a posteriori, le azioni svolte e individuare quali hanno
prodotto violazioni di sicurezza. Perciò è importante tenere traccia di chi ha effettuato le azioni.

**L’auditing consiste in un processo sistematico (= con delle regole prefissate, anche cadenze temporali), indipendente, documentato, svolto secondo regole precise prefissate, che controlla le azioni svolte per individuare le violazioni e suggerire rimedi.**

L'auditing perciò richiede:

- selezione e salvataggio delle azioni pericolose
- protezione dei file di log
- autenticazione degli utenti in modo da poter collegare le azioni pericolose a chi le ha compiute.

## Obblighi legislativi

### Informazioni

Legislativamente le informazioni sono categorizzate in:

- **personali**: dati che rendono identificabile una persona. A loro volta divisi in:
  - **comuni**: permettono di individuare una persona (come nome, telefono, nome utente contenente il vero nome, ecc.);
  - **sensibile**: che possono rivelare orientamento sessuale, politico, salute, ecc..
- **di macchina**: prodotti dai sensori che operano in una macchina. Possono essere oggetto di contratto tra il produttore della macchina e il compratore ed utilizzatore della macchina. Queste informazioni non sono regolate da legislatura particolare, ma possono essere comunque considerate confidenziali per motivi aziendali.

La legislazione europea, attraverso il GDPR (General Data Protection Regulation, legge europea 679/2016), definisce responsabilità e procedure operative a cui gli utilizzatori di dati personali devono sottostare (quindi non si regolano le informazioni di macchina).

Nello specifico occorre definire gli scopi di utilizzo delle informazioni personali ed anonimizzare le stesse in caso se ne faccia utilizzo diverso da quello dichiarato. Esempio: se per identificare una persona in un sistema informatico aziendale si creano utenze con nomi che contengono sufficienti informazioni da ritracciare la persona, il nome utente diventa un dato personale. Se poi si occorre fare logging tale dato dovrà essere anonimizzato in modo tale che non sia alla portata di chiunque mappare l'alias con l'utenza.

