# Reti

## Tipi di reti

### Rete di overlay

Rete **overlay**: rete "sovrapposta" ad un'altra esistente, ovvero una rete che mette in comunicazione diretta varie entità in modo tale che queste non abbiano a che fare con eventuali filtri presenti tra di loro. Un esempio classico ne sono le VPN.

### VPN (Virtual Private Network)

Una VPN è una rete locale virtuale che collega sistemi operativi solitamente non raggiunti dalla stessa rete locale fisica.

Esistono due **tipi di VPN**:

- site-to-site VPN: sono VPN instaurate tra due reti, per esempio tra due sedi distaccate di una stessa azienda per le quali si vuole usare una rete privata unica;
- remote access VPN: VPN che collega singoli computer. Questa tecnologia è critica sotto il punto di vista della sicurezza, perché tale rete collega computer che non sono amministrati da una stessa entità quindi con una probabilità di essere infetti più alta.

## Servizio DNS

Il servizio DNS permette di individuare l'indirizzo di rete di un dispositivo partendo dal suo nome. Molto utile per gli umani, visto che riescono a ricordare meglio i nomi che sequenze numeriche.

Una rete che utilizza il servizio DNS può essere suddivisa in **zone**, ciascuna dei quali fa capo ad un unico server DNS. I dispositivi di una zona sono accomunati da una parte del nome uguale per tutti, che sarebbe appunto il nome della zona DNS.

Le zone possono essere annidate ed i server delle zone padre possono delegare le richieste a quelli delle zone figlie, se presenti. Questo permette di:

- dividere il carico di lavoro, onde evitare sovraccarichi;
- se la definizione delle zone segue la conformazione geografica, quindi stati considerati i tempi di trasmissione medi della rete, e sono stati dispiegati più server in modo tale che il servizio DNS sia velocemente raggiungibile da ogni punto della rete, permette di ridurre il tempo di accesso al servizio DNS (per ridurre i tempi di risoluzione il protocollo DNS predisponde una serie di cache nelle varie entità che entrano in gioco);
- contattare soltanto l'amministratore del DNS di zona per eventuali modifiche, che in teoria dovrebbe essere più semplice da raggiungere rispetto a quelli delle zone padre.

![Alcune zone DNS di esempio e relative macro-zone.](images/DNS - Zone.png)

## Firewall

Sistema che filtra i pacchetti provenienti da reti esterne, ==in modo tale che anche se al proprio interno la rete è configurata per gestire questi pacchetti le si impedisce di gestire quelli provenienti da reti esterne==.



==tutte le operazioni su rete richiedono che il firewall permetta il traffico MADDAI==

## Tecniche di rete

### NAT (Network Address Translation)

La tecnica NAT, descritta nell'RFC6131 e conosciuta anche come IP-masqueranding, permette di utilizzare un unico indirizzo per identificare un'intera rete, utilizzata ad esempio nel caso si vogliano interfacciare reti private ad internet. Il motivo per il quale si utilizza questa tecnica sono:

-  gli indirizzi IPv4 non sono sufficienti ad indirizzare tutti i dispositivi perché sono in numero minore;
- in termini di sicurezza conviene utilizzare uno spazio di indirizzi privato e non conosciuto all'esterno per gli host di una rete interna, piuttosto che utilizzare soltanto indirizzi pubblici, cosicché gli host della rete interna siano raggiungibili solo impostando il NAT;

Questa tecnica prevede che il router che interfaccia le reti cambi gli indirizzi e le porte dei pacchetti che lo attraversano, effettuando appunto una traduzione tra gli indirizzi della rete interna, ovvero la rete coperta da NAT, e indirizzi della rete esterna.

Le sostituzioni avvengono in questo modo:

- per i pacchetti in uscita:
  1. crea una porta sull'interfaccia sulla rete di destinazione che si riferisce all'indirizzo e porta del mittente e salva tale mapping nella tabella di NAT;
  2. sostituisce l'indirizzo del mittente del pacchetto con l'indirizzo della sua interfaccia sulla rete di destinazione e la porta sorgente con la porta creata al punto precedente;
- per i pacchetti in entrata:
  1. in base alla porta di destinazione del pacchetto ricevuto ricava l'indirizzo e la porta di destinazione sulla rete dalla tabella di NAT;
  2. sostituisce indirizzo e porta di destinazione con indirizzo e porta ricavati al punto precedente.

È quindi necessaria una tabella di NAT per ogni interfaccia del router, ognuna dei quali composta come segue:

| IP sorgente  | Porta sorgente | Protocollo | Porta assegnata | ...  |
| ------------ | -------------- | ---------- | --------------- | ---- |
| 192.168.1.45 | 4500           | TCP        | 1027            |      |

Per precisione indichiamo che se il NAT comprende anche le porte si chiama **PAT** (Port Address Translation) o network masquerading.

La **traslazione uno-ad-uno** prevede che ad ogni indirizzo della rete interna si associ un indirizzo della rete esterna. La **traslazione molti-ad-uno** prevede che gli indirizzi della rete interna siano mappati si un pool di indirizzi della rete esterna.

I binding che popolano la tabella di NAT possono essere creati soltanto da comunicazioni in uscita. Quando occorre pubblicare una porta di un host della rete interna occorre impostare una regola di **port forwarding**, ovvero un'associazione statica tra una porta sull'IP esterna ed un IP e porta di un host della rete interna.

Il NAT può essere fatto anche a più livelli.

Quando il NAT pubblica delle porte si deve ovviamente configurare il firewall di conseguenza.

Esistono vari tipi di NAT:

- statico: associare in modo statico un indirizzo della rete interna ad un indirizzo sulla rete esterna. Possiamo così esporre alla rete esterna soltanto tanti indirizzi della rete interna quanti quelli disponibili sulla rete esterna;
- dinamico: l'associazione tra indirizzi della rete interna e quelli disponibili sulla rete esterna avviene su necessità, rimuovendo l'associazione quando l'host della rete interna non necessita più di essere esposto;
- overloading: NAT e PAT insieme.

Drawbacks del NAT:

- NAT isola la rete e per renderne accessibili i servizi che vuole pubblicare tocca fa port-forwarding;

Tipi di NAT in funzione delle regole che implementano la comunicazione:

- full cone: i meno restrittivi, un certo indirizzo e porta mittente della rete sotto NAT è sempre mappato alla stessa combinazione di IP pubblico e porta. In ricezione tutto il traffico diretto a quella porta è inoltrato all'indirizzo nattato senza controllare l'indirizzo del mittente;
- restricted cone: come full cone, ma funziona solo con un altro indirizzo, sarà inoltrato all'host nattato solo ciò che proviene dal destinatario che aveva specificato in primo luogo. Rimane comunque il mapping fisso;
- port restricted cone: come restricted cone, ma restringe non solo sull'indirizzo del destinatario ma anche sulla porta. Rimane comunque il mapping fisso;
- symmetric: i più restrittivi, per ogni connessione il mapping utilizza una porta scelta random, oltre ad applicare le politiche del port restricted cone.

#### NAT traversal

Tecnologie che permettono di iniziare una comunicazione punto-punto tra due host coperti da NAT. Utilizzano principalmente due protocolli:

- STUN (Session Traversal of UPD through NAT devices);
- TURN (Traversal Using Relay NAT): protocollo che svolge ruolo di relay, utile a superare NAT simmetrici;
- ICE (Interactive Connectivity Establishment): protocollo che a forza di tentativi cerca di capire il tipo di firewall da superare. Utilizza sia STUN che TURN.

Playground della spiegazione: due peer A e B, ognuno dietro NAT e firewall, vogliono comunicare in realtime tramite canale UDP. Entrambi hanno accesso ad un server di rendezvous esterno alle reti in cui si trovano, quindi esiste un canale di segnalazione tra i due peer.

Sequenza messaggi:

1. A comunica al SIP proxy, un server di coordinamento, che vuole comunicare con B;
2. B riceve la richiesta di comunicazione e risponde indicando il suo indirizzo nella sua sottorete, sempre inviando la risposta al SIP proxy, che la inoltra ad A;
3. A risponde a B indicando il suo indirizzo nella sua sottorete, sempre tramite SIP proxy;
4. Comincia la comunicazione vera e propria tra i due peer: A invia un pacchetto a B - Il pacchetto raggiunge il router della sottorete di A e viene inoltrato nella rete successiva, ma gli viene cambiato l'indirizzo per via del NAT sotto il quale è la sua sottorete. Ad un certo punto, tra i vari hop della rete intermedia, il pacchetto sarà droppato perché l'indirizzo di B non è pubblicamente accessibile.
5. Proviamo con STUN: A contatta il rendezvous server chiedendogli quale sia il suo indirizzo IP. Il server gli risponde. Cambia lo scenario in base al NAT dietro il quale si trova A:
   - full cone: la porta per la richiesta rimane aperta e mappata ad A, abbiamo quindi una porta aperta sul NAT. A manda a B la porta appena aperta ed il suo indirizzo IP pubblico attraverso il server proxy, e B si adopera. Anche B è dietro full cone. A risponderà poi al mittente del pacchetto ricevuto, che non sarà l'indirizzo di B nella sua sottorete ma un indirizzo a cui B può essere raggiunto perché visto dalla rete esterna a quella di A.
   - se il pacchetto proveniente da B viene rifiutato allora potrebbe essere un restricted cone. A richiede, sempre dalla stessa porta in modo da testare che il mapping rimanga lo stesso, direttamente a B; non mi interessa che arrivi o meno, mi interessa solo istruire il NAT ad accettare la roba proveniente da B. Ora B prova a comunicare e riesce, quindi abbiamo "bucato" il restricted cone NAT;
6. Se STUN non va, utilizziamo un server di relay con il protocollo TURN perché entrambi sono dietro symmetric NAT, quindi la roba inviata da B viene droppata dal NAT davanti A. Il server TURN sarà utilizzato come relay per tutta la comunicazione.

Il protocollo ICE indica come utilizzare STUN e TURN per creare questo tunnel. Utilizzato in WebRTC per instaurare comunicazioni dirette tra i browser client.

### SSH port forwarding

Si crea un **tunnel SSH**, ovvero un canale tramite SSH che collega la porta di un sistema operativo alla porta di un altro. Essendo una procedura abbastanza ortodossa sotto l'aspetto della sicurezza, visto che elude le regole dei firewall che proteggono i s.o., se ne consiglia l'utilizzo soltanto temporaneo.

Per crearlo è necessario che su entrambi le istanze di s.o. sia installato SSH, client sul s.o. che inizia la connessione e server su quello che riceve la richiesta di creazione del tunnel. Nel caso che nessuno dei due s.o. può fare da server ma entrambi possono iniziare una connessione SSH è possibile utilizzare una terza istanza di s.o. con server SSH installato e raggiungibile come **relay**.

#### SIP

SIP e NAT: NAT cambia gli indirizzi del pacchetto, quindi il contenuto appartiene ad una sottorete e il pacchetto ad un'altra, il che è na roba un po' stronza.

## Reti in Linux

==ci vorrebbe un ripasso del networking in Linux, sopratutto su come sono gestite le interfacce e come ereditano tutte le impostazioni da 0.0.0.0 o 127.0.0.1 o su cosa sia il binding di una porta ad un IP==

### iptables e netfilter

netfilter è un modulo del kernel Linux, incluso di default dalla versione 2.4, che si occupa di instradare tutti i pacchetti di rete che passano nella macchina, sia internamente alla macchina, da o verso l'esterno esterno o semplicemente in caso siano da redirezionare (come se la macchina fosse un router). Nello specifico le operazioni che permette di fare sui pacchetti sono:

- raw: operazioni generali;
- mangling: modificare del contenuto dei pacchetti, anche a bassissimo livello;
- filter: lasciar proseguire o meno un pacchetto;
- NAT: modificare di indirizzo e porta del mittente o di destinazione.

netfilter divide la vita del pacchetto nella macchina in questione in fasi, chiamate "catene":

- prerouting: azioni su pacchetti che provengono dall'esterno;
- input: azioni sui pacchetti che sono diretti ai processi locali ("Local Process" nella @fig:netfilter_catene);
- output: azioni sui pacchetti generati dai processi locali e destinati all'esterno;
- forward: azioni sui pacchetti che non sono diretti ai processi locali;
- postrouting: azioni sui pacchetti diretti verso l'esterno.

![Catene di netfilter.](images/netfilter - Catene.svg){#fig:netfilter_catene}

netfilter permette di definire delle regole ad applicare ad una specifica catena e tabella relativa all'operazione; in base all'operazione saranno disponibili diverse tabelle tra raw, mangle, nat e filter.

Per definire le regole secondo il quale netfilter opera si utilizzano la utility di userspace "iptables", per i pacchetti IPv4, ed "ip6tables", per i pachetti IPv6.

#### Esempi comandi iptables

Esempio: mostra le regole definite per la tabella "filter" (con IP in rappresentazione numerica):

```
~> sudo iptables --t filter --list --numeric
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         

Chain FORWARD (policy DROP)
target     prot opt source               destination         
DOCKER-USER  all  --  0.0.0.0/0            0.0.0.0/0           
DOCKER-ISOLATION-STAGE-1  all  --  0.0.0.0/0            0.0.0.0/0           
ACCEPT     all  --  0.0.0.0/0            0.0.0.0/0            ctstate RELATED,ESTABLISHED
DOCKER     all  --  0.0.0.0/0            0.0.0.0/0           
ACCEPT     all  --  0.0.0.0/0            0.0.0.0/0           
ACCEPT     all  --  0.0.0.0/0            0.0.0.0/0           

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         

Chain DOCKER (1 references)
target     prot opt source               destination         

Chain DOCKER-ISOLATION-STAGE-1 (1 references)
target     prot opt source               destination         
DOCKER-ISOLATION-STAGE-2  all  --  0.0.0.0/0            0.0.0.0/0           
RETURN     all  --  0.0.0.0/0            0.0.0.0/0           

Chain DOCKER-ISOLATION-STAGE-2 (1 references)
target     prot opt source               destination         
DROP       all  --  0.0.0.0/0            0.0.0.0/0           
RETURN     all  --  0.0.0.0/0            0.0.0.0/0           

Chain DOCKER-USER (1 references)
target     prot opt source               destination         
RETURN     all  --  0.0.0.0/0            0.0.0.0/0   
```

Nell'esempio si possono vedere anche ulteriori catene definite da Docker.

Esempio: non inoltrare nessun pacchetto.

```
# --policy CHAIN TARGET: change policy on CHAIN to TARGET
~> sudo iptables --table filter --policy FORWARD DROP
```

Esempio: aggiungere una regola ai pacchetti che destinati all'esterno (quindi da definire nella catena di postrouting) e che escono dall'interfaccia eth0 tale da modificare l'indirizzo (quindi occorre metterla nella tabella nat) mittente (quindi effettua una SourceNAT) dei pacchetti TCP in uno di quelli tra "194.236.50.155" e "194.236.50.160" e con una porta tra "1024" e "32000".
Questa è una regola di masquerading, ovvero che cambia la sorgente del pacchetto, come fanno i router.

```
~> iptables \
	--append POSTROUTING \  # --append CHAIN: append to chain
	--out-interface eth0 \
	--table nat \
	--jump SNAT \  # --jump TARGET: target for rule
	--protocol tcp \
	--to-source 194.236.50.155-194.236.50.160:1024-32000
```
