/* server.c */

       #ifndef _GNU_SOURCE
       #define _GNU_SOURCE  /* To get defns of NI_MAXSERV and NI_MAXHOST */
       #endif

       #include <arpa/inet.h>
       #include <sys/socket.h>
       #include <netdb.h>
       #include <ifaddrs.h>
       #include <stdio.h>
       #include <stdlib.h>
       #include <unistd.h>
       #include <string.h>
       #include <linux/if_link.h>


#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>		/* fd_set */
#include <netinet/in.h>		/* sockaddr_in{} and other Internet defns */
#include <arpa/inet.h>		/* inet(3) functions */
#include <errno.h>
#include <signal.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>		/* bzero */
#include <unistd.h>
#include <fcntl.h>	/* for nonblocking */


#define DEFAULTCOLOR "\033[0m"
#define ROSSO  "\033[22;31m"
#define VERDE  "\033[22;32m"

#define GREEN "\033[0;0;32m"
#define WHITE   "\033[0m"
#define RED "\033[0;0;31m"
#define BLU "\033[0;0;34m"
#define ORANGE "\033[0;0;33m"


typedef struct sockaddr SA;
#define MAXBUFLEN 1000
#define LENBUFAGGIUNTOINTESTA 2
#define STRLENBUFAGGIUNTOINTESTA "2"

typedef struct { 
	char buf[MAXBUFLEN];
	int first;
	int len;
} buffer;

typedef struct stato {
	int nready; /* risultato select */
	int maxfd;
    	fd_set Rset;
	fd_set Wset;
	int listenfd;
    	int maxi;
    	int client[FD_SETSIZE];
    	buffer bufs[FD_SETSIZE];
} Stato;

char if_inet_address[NI_MAXHOST+1];
char myInternalName[1024];
unsigned int counter=0;


       int get_if_address(char *ifname, char if_inet_address[NI_MAXHOST+1])
       {
           struct ifaddrs *ifaddr, *ifa;
           int family, s, n;
           char host[NI_MAXHOST];
           int result=0;

           if (getifaddrs(&ifaddr) == -1) {
               perror("getifaddrs");
               return(result);
           }

           /* Walk through linked list, maintaining head pointer so we
              can free list later */

           for (ifa = ifaddr, n = 0; ifa != NULL; ifa = ifa->ifa_next, n++) {
               if (ifa->ifa_addr == NULL)
                   continue;

               family = ifa->ifa_addr->sa_family;

               if (family == AF_INET /* || family == AF_INET6 */  ) {
                   s = getnameinfo(ifa->ifa_addr,
                           /*
                           (family == AF_INET) ? sizeof(struct sockaddr_in) :
                           */
                                                 sizeof(struct sockaddr_in6),
                           host, NI_MAXHOST,
                           NULL, 0, NI_NUMERICHOST);
                   if (s != 0) {
                       printf("getnameinfo() failed: %s\n", gai_strerror(s));
                       break;
                   }
                   else
                   {
                       /*
                       printf("%s\n", host);
                       */
                       if( strncmp( ifa->ifa_name, ifname, NI_MAXHOST )==0 ) {
			  memset(if_inet_address, 0, NI_MAXHOST+1 );
                          snprintf(if_inet_address, NI_MAXHOST+1, "%s", host);
                          result=1;
                          break;
                       }
                   }
               }
           }

           freeifaddrs(ifaddr);
           return(result);
       }


int set_non_blocking(int socketfd) {
	int flags;
	if ( (flags=fcntl(socketfd,F_GETFL,0)) <0 ) {
		printf ("fcntl(F_GETFL) failed, Err: %d \"%s\"\n", errno,strerror(errno));
		return(0); /* errore */
	}
	flags |= O_NONBLOCK;
	if ( fcntl(socketfd,F_SETFL,flags) <0 ) {
		printf ("fcntl(F_SETFL) failed, Err: %d \"%s\"\n", errno,strerror(errno));
		return(0); /* errore */
	}
	return(1);
}

int set_blocking(int socketfd) {
	int flags;
	if ( (flags=fcntl(socketfd,F_GETFL,0)) <0 )  {
		printf ("fcntl(F_GETFL) failed, Err: %d \"%s\"\n", errno,strerror(errno));
		return(0); /* errore */
	}
	flags &= (~O_NONBLOCK);
	if ( fcntl(socketfd,F_SETFL,flags) <0 )  {
		printf ("fcntl(F_SETFL) failed, Err: %d \"%s\"\n", errno,strerror(errno));
		return(0); /* errore */
	}
	return(1);
}


int setup_socket_listening(short int local_port_number) {
	int listenfd, ris, OptVal;
   	 struct sockaddr_in servaddr;

    	listenfd = socket(AF_INET, SOCK_STREAM, 0);
    	if(listenfd<0)
    	{	
		printf ("socket() failed\n");
		return(-1);
    	}
 
	/* avoid EADDRINUSE error on bind() */
	OptVal = 1;
	ris = setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, (char *)&OptVal, sizeof(OptVal));
	if (ris < 0 )  {
		printf ("setsockopt() SO_REUSEADDR failed, Err: %d \"%s\"\n", errno,strerror(errno));
		return(-1);
	}
 
    	bzero(&servaddr, sizeof(servaddr));
    	servaddr.sin_family = AF_INET;
    	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    	servaddr.sin_port = htons(local_port_number);
    	ris = bind(listenfd, (SA *) &servaddr, sizeof(servaddr));
    	if(ris<0) 
    	{	
		perror ("bind() failed: ");
		close(listenfd);
		return(-1);
    	}
    	ris = listen(listenfd, 100);
    	if(ris<0) 
    	{	
		perror ("listen() failed: ");
		close(listenfd);
		return(-1);
    	}
	return(listenfd);
}

void init_stato( Stato *pS, int listenfd) {

	int i;

	pS->listenfd = listenfd; /* initialize */
	pS->maxi = -1; /* index into client[] array and into Rset and Wset */
	for (i = 0; i < FD_SETSIZE; i++) {
	    	pS->client[i] = -1; /* -1 indicates available entry */
		pS->bufs[i].len=0; /* no bytes */
		pS->bufs[i].first=0;
	}
}

void setup_before_select(Stato *pS) {

	int i;

	FD_ZERO( &(pS->Rset) );
	FD_ZERO( &(pS->Wset) );
	FD_SET(  pS->listenfd, &(pS->Rset) );
	pS->maxfd=pS->listenfd;

	for (i=0; i<=pS->maxi; i++) {
		if( pS->client[i] >= 0 ) {
			if( pS->bufs[i].len==0 )  /* nulla da spedire, metto in lettura */
				FD_SET(pS->client[i], &(pS->Rset) );
			else /* roba da spedire, setto in scrittura */
				FD_SET(pS->client[i], &(pS->Wset) );

			if( pS->maxfd < pS->client[i] ) 
				pS->maxfd=pS->client[i];
		}
	}
}

int insert_new_client( Stato *pS, int connfd) {

	int i;

	for (i = 0; i < FD_SETSIZE; i++) {
	    if (pS->client[i] < 0) {
		    pS->client[i] = connfd; /* save descriptor */
			pS->bufs[i].len=0;
			pS->bufs[i].first=0;

			/*
		    	printf(VERDE "aggiunto client %d in posizione %d\n" DEFAULTCOLOR , connfd, i);
			fflush(stdout);
			*/

		    break;
	    }
    	}
    	if (i == FD_SETSIZE) 
    	{
	    	printf("too many clients");
	    	return(-1);
    	}
    	if (i > pS->maxi) pS->maxi = i; /* max index in client[] array */
		return(1);

}


int check_for_new_connection( Stato *pS ) {

    	int connfd;
    	socklen_t clilen;
    	struct sockaddr_in cliaddr;

    	if ( 	( pS->nready > 0 ) 
		 &&
		( FD_ISSET( pS->listenfd, &(pS->Rset) ) )
	   ) 
	{ /* new client connection */

		pS->nready--;

		clilen = sizeof(cliaddr);
	    	connfd= accept( pS->listenfd, (SA *) &cliaddr, &clilen);
		if( connfd<0 ) {
			if( errno==EINTR ) return(0);
			else {
				perror("accept() failed: STOP - :");
				exit(11);
			}
		}
		else {
			/* un po' di output */
			printf("new client n %i from %s %i \n", counter,
				inet_ntoa(cliaddr.sin_addr), 
				ntohs(cliaddr.sin_port)  );
			fflush(stdout);
			counter++;

			/* inserisco il client nelle mie strutture dati 
			   per gestire la sua connessione
			*/
			insert_new_client( pS, connfd );
		}

		return(1);
	}
	return(0);
}


void check_for_recv_send( Stato *pS ) {

	int i, n;

	for (i = 0; (i<=pS->maxi)&&(pS->nready>0); i++)  /* check all clients for data */
	{
		int sockfd;

		if ( (sockfd= pS->client[i]) < 0)
			continue;

		if (FD_ISSET(sockfd, &(pS->Rset))) { /* to be received */
			memset( pS->bufs[i].buf, 0, MAXBUFLEN );
			do {
		    		n = recv(sockfd, pS->bufs[i].buf, MAXBUFLEN, 0 );
			} while((n<0) && (errno==EINTR) );

			if(n==0) {
				/*connection closed by client */
				close(sockfd);
				pS->client[i] = -1;
		    	} else if(n<0) {
				/* connection error */
				printf("error detected from socket %d\n", sockfd);
				close(sockfd);
				pS->client[i] = -1;
			} else {
				/*
				char ch;
				*/
				int len;
				pS->bufs[i].buf[n]=0;

				/*
				printf(ROSSO "received from socket %d %s\n" DEFAULTCOLOR , n, pS->bufs[i].buf);
				*/

				/* inserisco in testa al buffer di risposta
				   fino a len +1 caratteri che contengono
				   la stringa puntata da myInternalName
				   e che identificano il worker node 
				   che risponde al client
				*/ 
				/* len=strlen( if_inet_address ); */
				len=strlen( myInternalName );
				memmove(pS->bufs[i].buf+len+2,pS->bufs[i].buf,n);
				/*
				ch=pS->bufs[i].buf[len+1];
				*/
				/* strncpy(pS->bufs[i].buf, if_inet_address, len+1); */
				strncpy(pS->bufs[i].buf, myInternalName, len+1);
				pS->bufs[i].buf[len]=' ';
				pS->bufs[i].buf[len+1]=' ';
				/*
				pS->bufs[i].buf[len+1]=ch;
				*/

				pS->bufs[i].len=n+len+2;
				pS->bufs[i].first=0;

				/* semplifico, spedisco sempre 1000 bytes */
				pS->bufs[i].len=MAXBUFLEN;
			}
   			pS->nready--;
			continue; /* se dovevo leggere non devo spedire */
		}

		if (FD_ISSET(sockfd, &(pS->Wset))) { /* to be sent */
			do {
		    		n = send(sockfd, &(pS->bufs[i].buf[pS->bufs[i].first]), pS->bufs[i].len, MSG_NOSIGNAL|MSG_DONTWAIT );
			} while((n<0) && (errno==EINTR) );

			if(n<0) {
				/* connection error */
				printf("error detected from socket %d\n", sockfd);
				close(sockfd);
				pS->client[i] = -1;
			} else {
				/*
				printf("bytes sent using socket %d\n", n);
				*/
				pS->bufs[i].len -= n;
				if(pS->bufs[i].len==0)  {
					pS->bufs[i].first=0;
					/*
					printf("packet sent back to the client using socket %d\n", sockfd);
					*/
				}
				else
					pS->bufs[i].first += n;
			}
			pS->nready--;
		}
		    
	} /* fine for(i=0,...) */
}


void usage (void)
{
    printf ("usage: tcprelay.exe LOCAL_PORT_NUMBER LocalNetworkInterfaceName [EnvVarName]\n");
    exit (1);
}

int main (int argc, char *argv[], char * envp[])
{

    short int local_port_number;
    int listenfd;
	Stato S;
    int i;
    char *pMyEnvVar=NULL;
    char *MyEnvVarName=NULL; /* ="MYVAR"; */
    
    /* solo per debug 
    int ris, myerrno;
    */
	
    if ( (argc!=4) && (argc != 3) )
    {
		printf ("necessari 2 o 3 parametri\n");
		usage ();
		exit (2);
    }
    else
    {
       int result;
       char *ifname;

	local_port_number = atoi (argv[1]);
       ifname=argv[2];
       result=get_if_address(ifname, if_inet_address);
       if(result==1) {
          printf("address %s\n", if_inet_address );
          fflush(stdout);
       } else {
          printf("get_if_address failed\n");
          fflush(stdout);
          exit(3);
       }
       if( argc==4 ) {
	  MyEnvVarName=argv[3];
	  strcat(MyEnvVarName,"=");
          /*
          printf("finding var %s \n", MyEnvVarName );
          fflush(stdout);
          */

          for (i = 0; envp[i] != NULL; i++) {
             /*
             printf("%s\n", envp[i]);
             fflush(stdout);
             */
             if( strncmp(envp[i], MyEnvVarName, strlen(MyEnvVarName)) == 0 ) {
             	pMyEnvVar=envp[i];
                pMyEnvVar=pMyEnvVar+strlen(MyEnvVarName);
           	/*
		printf("found var %s value %s\n", MyEnvVarName, pMyEnvVar ); 
                fflush(stdout);
		*/
                strcpy(myInternalName,pMyEnvVar);
                printf("found var %s value %s\n", MyEnvVarName, pMyEnvVar );
                fflush(stdout);

                strcat(myInternalName,"--");
                strcat(myInternalName,if_inet_address);
	   	break;
	     }
          }
          if( pMyEnvVar == NULL ) {
             printf("environment var %s not found \n", MyEnvVarName );
             fflush(stdout);
             exit(4);
          }
       } else { /* argc==3 */
          strcpy(myInternalName,"noneMyInternalName");
          strcat(myInternalName,"--");
          strcat(myInternalName,if_inet_address);
       }
    }


    listenfd=setup_socket_listening(local_port_number);
    if(listenfd<0) exit(5);

    init_stato(&S, listenfd);

    for ( ; ;  ) 
    {

		do {
			setup_before_select(&S);

				/* solo per debug 

				printf("PRIMA DI SELECT \n" );
				printf("maxfd+1= %d  Rset= ", S.maxfd+1);
				for (i=0; i<FD_SETSIZE; i++) 
				{
			 		if ( FD_ISSET( i, &(S.Rset) ) ) { printf("%d ", i); } 
				}
				printf("   Wset= ");
				for (i=0; i<FD_SETSIZE; i++) 
				{ 
					if ( FD_ISSET( i, &(S.Wset)) ) { printf("%d ", i); }
				}
				printf("\n");

				fine solo per debug */

			S.nready = select(S.maxfd+1, &(S.Rset), &(S.Wset), NULL, NULL);

					/* solo per debug

					myerrno=errno;
					printf("nready=%d\n", S.nready);
					errno=myerrno;

					fine solo per debug */

		} while( (S.nready<0) && (errno==EINTR) );

		if( S.nready<0) {
			perror("select failed: kill process ");
			fflush(stderr);
			exit(6);
		}


				/* solo per debug 

				printf("DOPO SELECT \n" );
				printf("maxfd+1= %d  Rset= ", S.maxfd+1);
				for (i=0; i<FD_SETSIZE; i++) 
				{
			 		if ( FD_ISSET( i, &(S.Rset) ) ) { printf("%d ", i); } 
				}
				printf("   Wset= ");
				for (i=0; i<FD_SETSIZE; i++) 
				{ 
					if ( FD_ISSET( i, &(S.Wset)) ) { printf("%d ", i); }
				}
				printf("\n");

				fine solo per debug */


		/* gestione richieste di connessione da client */
		check_for_new_connection(&S);


		/* gestione ricezione e spedizione dati con connessioni gia' instaurate */
		check_for_recv_send(&S);

    }  /* fine for(;;) */

    return(0);
}
    
    
    

