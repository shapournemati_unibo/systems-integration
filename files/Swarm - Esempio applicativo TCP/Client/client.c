/* client.c spedisce in parallelo stringa un byte alla volta, con ritardo,
 riceve stringa con aggiunta
 Man mano riceve byte in risposta e li stampa a video.

 su SunOS compilare con gcc -o cliTCP.c -lsocket -lnsl cliTCP.c
 su linux gcc -o cliTCP cliTCP.c

 eseguire ad esempio su 137.204.72.49 lanciando la seguente riga di comandi
 cliTCP 130.136.2.7 5001

 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h> /* per dnsquery */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdint.h> /* uintptr_t */
#include <inttypes.h> /* PRIiPTR */
#include <pthread.h>

#define SOCKET_ERROR ((int)-1)
#define SIZEBUF 1000

char string_hostname[100];
char string_remote_ip_address[100];
short int remote_port_number;
uintptr_t counter=0;


/* Description of data base entry for a single host.
 struct hostent
 {
 char *h_name; // Official name of host.
 char **h_aliases; // Alias list.
 int h_addrtype; // Host address type.
 int h_length; // Length of address.
 char **h_addr_list; // List of addresses from name server.
 };
*/

int dnsquery (char *name, char *OUT_IPaddress) {
    struct hostent *phostent;
    int x=0;

    if ((phostent = gethostbyname (name))==NULL) {
        herror ("il dns non riesce a risolvere il nome\n gethostbyname() failed:");
        exit (0);
    }

    while ( phostent->h_addr_list[x]!=NULL ) {
        sprintf (OUT_IPaddress, "%s", inet_ntoa ( * ( (struct in_addr*) (phostent->h_addr_list[x]) ) ) );
        return (1);
        /* uso solo il primo
        x++;
        */
    }

    return (0);
}

/* thread che spedisce e legge */
void *SendAndRecv (void *arg) {
#define MAXSIZE 4

    struct sockaddr_in Local, Serv;
    int socketfd, OptVal, ris;
    int n, i, len, counter;
    char msg[MAXSIZE];
    char bufricezione[SIZEBUF];
    uintptr_t indice;

    indice= (uintptr_t)arg;

    for (i=0; i<MAXSIZE-1; i++) msg[i]='a';

    msg[MAXSIZE-1]='\0';
    /*
    for(i=0;i<MAXSIZE-2;i++) msg[i]='a';
    msg[MAXSIZE-2]='\n';

    msg[MAXSIZE-1]='\0';
    */

    /* get a stream socket */
    /* printf ("socket()\n"); */
    socketfd = socket (AF_INET, SOCK_STREAM, 0);

    if (socketfd == SOCKET_ERROR) {
        printf ("socket() failed, Err: %d \"%s\"\n", errno,strerror (errno));
        exit (1);
    }

    /* avoid EADDRINUSE error on bind() */
    OptVal = 1;
    /* printf ("setsockopt()\n"); */
    ris = setsockopt (socketfd, SOL_SOCKET, SO_REUSEADDR, (char *)&OptVal, sizeof (OptVal));

    if (ris == SOCKET_ERROR) {
        printf ("setsockopt() SO_REUSEADDR failed, Err: %d \"%s\"\n", errno,strerror (errno));
        exit (1);
    }

    /* name the socket */
    memset ( &Local, 0, sizeof (Local) );
    Local.sin_family = AF_INET;
    /* indicando INADDR_ANY viene collegato il socket all'indirizzo locale IP */
    /* dell'interaccia di rete che verr� utilizzata per inoltrare i dati */
    Local.sin_addr.s_addr = htonl (INADDR_ANY); /* wildcard */
    Local.sin_port = htons (0);
    /* printf ("bind()\n"); */
    ris = bind (socketfd, (struct sockaddr*) &Local, sizeof (Local));

    if (ris == SOCKET_ERROR) {
        printf ("bind() failed, Err: %d \"%s\"\n",errno,strerror (errno));
        exit (1);
    }

    /* assign our destination address */
    memset ( &Serv, 0, sizeof (Serv) );
    Serv.sin_family = AF_INET;
    Serv.sin_addr.s_addr = inet_addr (string_remote_ip_address);
    Serv.sin_port = htons (remote_port_number);

    /* connection request */
    /* printf ("connect()\n"); */
    ris = connect (socketfd, (struct sockaddr*) &Serv, sizeof (Serv));

    if (ris == SOCKET_ERROR) {
        printf ("connect() failed, Err: %d \"%s\"\n",errno,strerror (errno));
        exit (1);
    }

    /* printf ("dopo connect()\n"); */
    fflush (stdout);


    /* scrittura */
    for ( counter=0; counter<2; counter++) {
        len = MAXSIZE;
        /*
        printf ("counter %d write()\n", counter);
        fflush(stdout);
        */
        n=send (socketfd, msg, MAXSIZE, MSG_NOSIGNAL);

        if (n<0) {
            char msgerror[1024];
            /* VITTORIO, PEZZO DI COGLIONE, CONTROLLA SE c'e' EINTR in errno !!!!! */
            sprintf (msgerror,"write() failed [err %d] ",errno);
            perror (msgerror);
            fflush (stdout);
            exit (1);
        } else if (n!=MAXSIZE) {
            printf ("%i bytes sent only. quit\n", n);
            fflush (stdout);
            exit (1);
        } else {
            int nread=0;
            printf ("thread %" PRIiPTR " sent %d bytes\n", indice, n);
            fflush (stdout);

            /* QUESTO PEZZO LEGGE I MESSAGGI RICEVUTI
            */
            len=n;
            /* SEMPLIFICAZIONE, LEGGO SEMPRE 1000 bytes */
            len=SIZEBUF;

            do {
                do {
                    n=recv ( socketfd, bufricezione+nread, SIZEBUF/nread, MSG_NOSIGNAL);
                } while ( (n<0) && (errno==EINTR) );

                if (n==0) {
                    printf ("connection closed\n");
                    exit (0);
                } else if (n<0) {
                    printf ("recv error\n");
                    exit (0);
                } else {
                    len-=n;
                    nread+=n;
                }
            } while (len>0);

            printf ("thread %" PRIiPTR " read %d bytes \"%s\" \n", indice, n, bufricezione );

            /* FINE DEL PEZZO CHE LEGGE I MESSAGGI RICEVUTI */

        }

        if ( counter< (2-1) )
            sleep (30);

    } /* for */

    /* chiusura */
    close (socketfd);

    pthread_exit (NULL);
    return (0);
}

void usage (void) {
    printf ("usage: cliTCP REMOTE_IP_NUMBER|REMOTEHOSTNAME REMOTE_PORT_NUMBER NUMBEROFTHREADS\n");
    fflush (stdout);
    exit (1);
}

int main (int argc, char *argv[]) {
    int numthreads;
    pthread_t tid;
    int rc;
    intptr_t t;


    if (argc!=4) {
        printf ("necessari 3 parametri\n");
        usage();
        exit (1);
    }

    /* strncpy(string_remote_ip_address, argv[1], 99);
    */
    strncpy (string_hostname, argv[1], 99);

    if ( dnsquery (string_hostname, string_remote_ip_address) !=1 ) {
        printf ("failed dsn query for hostname %s \n", string_hostname);
        exit (1);
    }

    printf ("server IP address %s\n", string_remote_ip_address);
    fflush (stdout);
    remote_port_number = atoi (argv[2]);
    numthreads = atoi (argv[3]);

    for (t=0; t < numthreads; t++) {

        /* printf("Creating thread %" PRIiPTR "\n", t); */
        rc = pthread_create (&tid, NULL, SendAndRecv, (void*)t );

        if (rc) {
            printf ("ERROR; return code from pthread_create() is %i \n",rc);
            fflush (stdout);
            exit (1);
        }

        /* creo i threads un po- alla volta */
        if ( t%10==9 ) {
            printf ("sleep 1 berfore Creating threads\n");
            fflush (stdout);
            sleep (1);
        }
    }

    printf ("Created %i threads \n", numthreads);
    fflush (stdout);

    pthread_exit (NULL);
    return (0);
}

