# Cloud

Il **cloud di un provider** è essenzialmente un gruppo di datacenter. Le macchine dei singoli datacenter cooperano per fornire dei micro-servizi.

I tipi di micro-servizi disponibili in cloud differiscono per cosa è reso disponibile all'utente, e sono:

- *SaaS* (Software as a Service): il micro-servizio è un'applicazione a se stante. Ad esempio Google Docs;
- *PaaS* (Platform as a Service): il micro-servizio fornisce un'API di sviluppo, solitamente accessibile attraverso un URL. Ad esempio, il database CosmosDB;
- *IaaS* (Infrastrucuture as a Service): fornisce una infrastruttura su cui installare servizi come, ad esempio, una macchina virtuale.

Come il micro-servizio sia realizzato dipende dal provider, ma in linea di massima sono eseguiti su un'architettura pensata per fornire determinate prestazioni in base alle richieste reali, ovvero **scalare**. Le architetture software di gestione delle macchine del datacenter sono chiamate **CMP (Cloud Management Platform)**; le principali sono OpenNebula e OpenStack.

Le prestazioni fornite ai singoli utenti e quelle massime complessive sono solitamente determinate da quanto si paga per il suddetto micro-servizio.
