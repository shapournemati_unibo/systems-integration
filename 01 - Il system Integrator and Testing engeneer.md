# Il "System integrator and Testing Engeneer"

Il termine più corretto per individuare il profilo del sistemista moderno è "Systems Integration and Testing Engeneer" ed è definita dall'EUCIP nel [questo documento](files/EUCIP - Systems Integration and Testining Engeneer v2.4.pdf). Il suo scopo è quello di progettare e realizzare sistemi complessi di software o **componenti** che interagiscono attraverso canali di comunicazione noti e con interfacce standard.

Gli standard possono essere:

- *de jure*: legalmente riconosciuti, ovvero previsti in letteratura;
- *de facto*: attualmente in voga;

e nello specifico riguardano:

- protocolli di rete;
- sicurezza nei sistemi di rete;
- programmazione e scripting;
- IDM (Identity Management Systems, Sistemi di Gestione dell'Identità);
- sistemi di provisioning (= processo mediante il quale un amministratore di sistema assegna risorse e privilegi);
- architetture a micro-servizi e sviluppo;
- gestione centralizzata di servizi distribuiti;
- sistemi di virtualizzazione;
- amministrazione di specifici sistemi operativi per desktop e server;
- amministrazione di sistemi cloud privati.

Il **systems integration architect** è una declinazione del system integration and testing engeneer e si occupa soltanto della progettazione dell'infrastruttura software.