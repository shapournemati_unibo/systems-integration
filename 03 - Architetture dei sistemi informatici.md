# Architetture dei sistemi informatici

## Sistemi a micro servizi

Un'applicazione è detta **a micro-servizi** se è composta da componenti loosely coupled (= separabili), personalizzabili e che si scambiano messaggi. Tali micro-servizi possono essere in esecuzione su uno stesso host o in host differenti ma collegati tra loro. Esempi di micro-servizi: una macchina virtuale, un'istanza di DBMS, un server Active Directory, una CDN (Content Delivery Network), un bus per lo scambio di messaggi.

Il contrario di un'applicazione a micro-servizi è un'applicazione monolitica. Un esempio di applicazione monolitica è un'applicazione java che accede ad un database: è composta da 2 componenti distinte che però non possono essere separate.

![Architettura monolitica ed a micro-servizi di un applicativo web che interagisce con un database.](images/Applicativo monolitico vs. applicativo micro-servizi.svg)

I vantaggi di un applicativo a micro-servizi sono:

- riutilizzabilità delle singole componenti, anche contemporaneamente da molteplici altri componenti;
- scalabilità: è possibile avere più istanze di un singolo componente, con la possibilità che le richieste siano suddivise tra le varie istanze (*bilanciamento del carico*);
- deployment: le istanze delle componenti possono essere eseguite anche su macchine differenti, risultando ideali allo sviluppo di applicativi orientati nativamente al cloud computing.

Gli svantaggi sono:

- occorre attenersi agli standard di intercomunicazione;
- assicurare un canale che permetta il passaggio di tali messaggi;

### Comunicazioni tra micro-servizi

Un problema noto degli applicativi a micro-servizi è come instradare le comunicazioni tra le diverse istanze degli stessi (indicate in rosso nella figura) non sapendo a priori quante ve ne saranno a runtime.

![Comunicazioni in un esempio di applicativo a micro-servizi eseguiti tutti nello stesso host.](images/Applicativo micro-servizi - Comunicazioni locali.svg)

Per questi scenari si utilizzano **bus di messaggi di tipo publish/subscribe orientati ai topic**. Questi sistemi di comunicazione prevedono che ogni entità (denominata *subscriber*) si iscriva al bus (o *message broker*) e dichiari i topic del quale vuole ricevere i messaggi. Le entità successivamente possono inviare messaggi dichiarandone il topic e saranno notificate soltanto dei messaggi spediti da altre entità con e dei topic di interesse.

I protocolli più utilizzati da questi bus sono:

- AMQP;
- MQTT.

Le implementazioni più diffuse di tali message broker sono:

- RabbitMQ: opensource, implementa entrambi i protocolli ed eseguibile anche su macchine esterne per far colloquiare componenti eseguite su macchine differenti. Implementa livelli di sicurezza;
-  Microsoft Azure Service Bus Messaging: implementa entrambi i protocolli e livelli di sicurezza.

