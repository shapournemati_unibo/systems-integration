# Il tempo reale in un computer

## L'orologio di un computer

Per **orologio** o clock si intende un meccanismo che permette di tracciare lo scorrere del tempo reale in una macchina, il quale non avrebbe altrimenti concezione di cosa sia il tempo reale. Questo meccanismo è composto da:

- un hardware clock o CMOS clock: [circuito RTC](https://it.wikipedia.org/wiki/Real-time_clock) che permette di tenere traccia dello scorrere del tempo sia che la macchina sia accesa che spenta (nei computer consumer è solitamente integrato nella scheda madre);
- un software clock, ovvero una software, solitamente integrato nel s.o., che converte il segnale del clock hardware nella rappresentazione temporale utilizzata dall'uomo e manutene il clock hardware assicurandosi.

La frequenza di avanzamento di un circuito RTC è regolata da un oscillatore al quarzo, la quale frequenza di oscillazione può fluttuare a causa di fattori esterni, come la tensione di alimentazione le caratteristiche ambientali; solitamente si discosta di qualche secondo al giorno. Per questo il la frequenza può essere modificata; l'operazione di ri-taratura è comunemente chiamata **"adjust"**.

Il kernel Linux permette di modificare il valore dell'hardware clock con il comando `hwclock`.

```shell
# hwclock --show  # mostra il valore corrente dell'hardware clock convertito al fuso orario corrente
2020-03-18 16:24:25.968078+01:00

# hwclock --systohc  # imposta l'hardware clock al valore corrente del software clock
```

Il software clock è calcolato ed incrementato dal s.o. a partire dall'orologio hardware. Il funzionamento tipico di un software clock prevede i seguenti passi:

1. al boot il s.o. assegna al software clock il valore dell'hardware clock;
2. ad ogni tick dell'hardware clock il valore è letto e convertito in quello del software clock. Questa conversione tiene conto di svariati altri fattori, quindi può introdurre un *offset* per rendere più affidabile il valore dell'orologio software.

## Sincronizzazione degli orologi

Lo scopo della sincronizzazione degli orologi di diverse macchine è assicurare il loro s.o. ed i processi che esegue abbiano la stessa nozione del tempo che scorre, ovvero avanzino alla stessa velocità.

L'importanza della sincronizzazione degli orologi è più evidente negli applicativi di rete, in quanto il tempo reale è parte integrante della loro logica e quindi ne può determinare il corretto funzionamento. Esempi notevoli sono i sistemi di sicurezza che concedono permessi per determinati periodi temporali (come Kerberos).

In particolare, la sincronizzazione può essere usata per stabilire una delle seguenti caratteristiche temporali:

- determinare l'istante in cui si verifica un evento;
- determinare il tempo trascorso tra più eventi;
- ordinare cronologicamente degli eventi.

Per determinare l'istante a cui si verifica un evento non è sufficiente il clock logico ma bisogna fare riferimento ai clock fisici e ai loro derivati software. Si può risolvere il problema in due modi:

- sincronizzazione interna di orologi: la macchina dispone di più clock fisici paritetici e l'obiettivo è minimizzare la differenza tra ogni coppia di clock, coordinandoli tra di loro a formare un clock (quasi) comune;
- sincronizzazione esterna di orologi: un clock sorgente esterno alla macchina fornisce il tempo reale. L'obiettivo è essere vicini a questo tempo sorgente il più possibile.

A meno di particolari situazioni *si utilizza sempre la sincronizzazione esterna*. Come orario di riferimento si utilizza l'**UTC (Universal Coordinated Time)**, che coincide con il fuso orario GMT (Greenwich Mean Time), a meno di approssimazioni infinitesimali.

La struttura di "distribuzione" dell'orario UTC prevede:

- il calcolo dell'UTC, che avviene al "Bureau International de l'Heure" sito a Parigi. Questi lo calcola mediando l'ora di 50 orologi atomici di Cesio 133 sparsi nel mondo;
- distribuzione dell'UTC agli utenti che necessitano di un tempo preciso con un segnale radio ad onde corte chiamato *WWV*. Grazie al fatto che il ritardo di trasmissione di un segnale radio rimane costante ed è proporzionale alla distanza dall'emittente riceventi possono determinare con accuratezza l'orario reale;
- distribuzione dell'UTC detenuto dai server del punto precedente a tutti gli altri che ne necessitano. Questo avviene tramite internet con l'utilizzo del protocollo *NTP*.

La problematica principale della sincronizzazione di orologi consiste nel *ritardo di trasmissione* del segnale di sincronizzazione dal master agli slave; è possibile misurarlo accuratamente soltanto prendendo come punto di riferimento uno stesso orologio, che sia del master o dello slave, altrimenti sarebbe alterato dallo sfasamento tra gli stessi. L'errore introdotto nell'orologio dello slave prende il nome di **errore di fase**.

### Metodi di sincronizzazione

I metodi di sincronizzazione che cercano di tamponare l'errore di fase sono *round-trip* e *tree-way*. Gli istanti "misurati" in questi metodi sono misurati secondo il clock locale alla macchina.

Tenere in mente che comunque entrambi i metodi presentati risentono comunque dei ritardi di trasmissione dei segnali di sincronizzazione, cosi come di malfunzionamenti generali del sistema (e.g.: segnale non recapitato, il master va in down, ...).

#### round-trip

Questo metodo richiede che sia lo slave a contattare il master per ottenere le informazioni di sincronizzazione. Il calcolo dell'errore di fase si basa sul **round trip time**, ovvero la somma del tempo impiegato dalla richiesta raggiungere il master e del tempo impiegato dalla risposta a raggiungere lo slave.

La successione di messaggi richiesta da questo metodo è la seguente:

- slave invia la richiesta, riportando nel messaggio il timestamp di invio del messaggio $T_0$;

- master riceve la richiesta e risponde con un messaggio contenente $T_0$ ed i timestamp di arrivo  $T_1$ e di invio della risposta $T_2$;

- alla ricezione della risposta lo slave memorizza il timestamp di arrivo della risposta $T_3$. Può così calcolare:

  - il delay di trasmissione: $$ D \approx \frac{(T_1 - T_0) + (T_3 - T_2)}{2} $$

  - l'offset tra i due orologi, ovvero il fattore di aggiustamento del suo orologio: $$ O \approx \frac{(T_1 - T_0) + (T_2 - T_3)}{2} $$

![Scambio di messaggi del metodo round-trip.](images/Sincronizzazione orologi - Messaggi metodo round-trip.svg){#fig:round-trip}

Questo metodo funziona bene se:

- il tempo di andata (in @fig:round-trip $T_1 - T_0$) e di ritorno (in @fig:round-trip $T_2 - T_3$) sono simili;
- i tempi di trasmissione sono abbastanza costanti nel tempo.

#### tree-way

Questo metodo richiede che sia il master ad iniziare la comunicazione. I passi previsti sono i seguenti:

- il master invia un messaggio allo slave in cui riporta l'istante di invio del messaggio $T_0$;

- lo slave risponde con un messaggio contenente $T_0$ ed i timestamp di arrivo  $T_1$ e di invio della risposta $T_2$;

- alla ricezione della risposta il master memorizza il timestamp di arrivo della risposta $T_3$. Può così calcolare:

  - il delay di trasmissione: $$ D \approx \frac{(T_1 - T_0) + (T_3 - T_2)}{2} $$

  - l'offset tra i due orologi, ovvero il fattore di aggiustamento del suo orologio: $$ O \approx \frac{(T_1 - T_0) + (T_2 - T_3)}{2} $$

  Delay ed offset sono successivamente inviati allo slave.

- Alla ricezione del messaggio lo slave corregge di conseguenza il suo clock sottraendogli l'offset comunicato.

![Messaggi scambiati dal metodo tree-way.](images/Sincronizzazione orologi - Messaggi tree-way.svg)

==Non ho capito perché sto metodo possa essere più preciso del precedente.==

### NTP (Network Time Protocol)

NTP è un protocollo di livello applicativo per la sincronizzazione esterna di orologi studiato per essere utilizzato su reti a commutazione di pacchetto (e.g. internet), ove l'errore di fase può potenzialmente variare di continuo per via del fatto che il percorso di un pacchetto può essere sempre differente. Si occupa quindi di definire lo scostamento tra l'orologio locale e un orologio esterno.

NTP definisce un orario come **ere + secondo dell'era**. Un'era è un periodo di 136 anni; l'era attuale ha indice 0 e va dal 1 Gennaio 1900 fino al 8 Febbraio 2036 escluso, mentre le ere precedenti hanno indice più piccolo (quindi negativo) e le ere successive hanno indice maggiore di zero.

L'attuale versione di NTP è la 4 ed è definita principalmente nell'RFC 5905 "Network Time Protocol Version 4: Protocol and Algorithms Specification". Secondo le specifiche il protocollo viaggia su UDP ed la porta assegnata ai server è la 123.

NTP definisce la presenza di server e client, dove i primi forniscono il tempo corretto ai secondi. I server sono divisi per **stratum**:

- i server di stratum 1 non sono pubblici e si sincronizzano con una determinata lista di server di stratum 2, per evitare sovraccarichi che compromettano il loro fuzionamento corretto. Loro sincronizzano il proprio orario da sistemi attendibili (orologi atomici, segnali radio e GPS);
- i server di stratum 2, 3, ... sono pubblici.

Ovviamente i server di stratum minore sono più precisi.

Esistono quattro **modalità di funzionamento** di NTP:

- client: la macchina è un client che richiede sincronizzazione ad un server NTP. Il client utilizza il metodo round-trip per calcolare l'offset di correzione dell'orologio;
- server: la macchina è un server che fornisce sincronizzazione a più client. I server possono essere soltanto di stratum 1;
- simmetrica: la macchina svolge sia il ruolo di client, rispetto ad alcuni server NTP da cui riceve la sincronizzazione, e di server, rispetto altri client a cui propaga la sincronizzazione;
- broadcast: un server manda pacchetti in multicast verso i client che si sono precedentemente abbonati al server (nella specifica si parla di "autenticati"). Non potendo utilizzare il metodo round-trip per il calcolo dell'offset la sincronizzazione sarà meno precisa.

#### La modalità client

Il protocollo prevede che il client, ad intervalli variabili tra gli 8 minuti e le 36 ore, richieda di sincronizzare il proprio orologio a diversi server NTP.

La sincronizzazione avviene come segue:

- il client contatta più server NTP;
- per ogni risposta si calcola l'offset ed il delay;
- gli algoritmi di selezione e clustering decidono quali pacchetti scartare, mantenendo solo quelli più vicini al client e col cammino minore al loro relativo server di stratum 1;
- gli algoritmi di mitigazione effettuano una media pesata dei risultati mantenuti ed ordinano al sistema di gestione del clock software (indicato con "Clock Discipline Process" in @fig:NTP_schema_funzionamento_client);
- conseguentemente il processo di taratura del clock hardware (indicato con "Clock Adjust Process" in @fig:NTP_schema_funzionamento_client) aggiusterà di conseguenza la frequenza VFO del clock hardware.

![Schema di fuzionamento di un client NTP.](images/NTP - Schema di funzionamento client.png){#fig:NTP_schema_funzionamento_client}

#### Anatomia del pacchetto NTP

I campi di un pacchetto NTP sono i seguenti:

- Leap Indicator (LI) (2 bit unsigned integer): codice di avviso per un "leap second" da inserire o cancellare dall'ultimo minuto del giorno corrente. Si possono avere i seguenti valori:
  
  - 0: nessun avviso;
  - 1: l'ultimo minuto ha 61 secondi;
  - 2: l'ultimo minuto ha 59 secondi;
  - 3: condizione di allarme (orologio non sincronizzato).
  
- Version Number (VN) (3 bit unsigned integer): indica il numero di versione di NTP/SNTP. Un valore pari a 3 indica NTPv3, 4 indica NTPv4;
  
- Mode (3 bit unsigned integer): indica la modalità della comunicazione. I valori possibili sono:
  
  - 0: riservato;
  - 1: attiva simmetrica;
  - 2: passiva simmetrica;
  - 3: client;
  - 4: server;
  - 5: broadcast;
  - 6: riservato per i messaggi di controllo NTP;
  - 7: riservato per usi privati.
  
  In funzionamento client-server il client manda un pacchetto di modo 4 ad un server, e questi risponde con pacchetti di modo 5.
  
  In funzionamento simmetrico la comunicazione è di tipo uno ad uno, sfruttando pacchetti di modo 1 e 2.
  
- Stratum (Strat) (8 bit unsigned integer): indica lo stratum dell'orologio locale. I valori possibili sono:
  
  - 0: non specificato o non disponibile. Nella versione 4 del protocollo identifica un particolare messaggio inviato dal server al client chiamato Kiss-of-Death, utilizzato per informare il client che qualcosa non è andato come doveva;
  - 1 - 15: stratum 1 - 15;
  - 16-255: riservato.
  
- Poll interval (Pool) (8 bit unsigned integer): massimo intervallo in secondi tra due successivi messaggi. Il valore memorizzato nel campo è in potenza di 2, ovvero $2^{intervallo}$;

- Precision (Prec) (8 bit signed integer): la precisione in secondi dell'orologio locale. Il valore memorizzato nel campo è in potenza di 2, ovvero $2^{precisione}$;

- Root Delay (32 bit signed fixed decimal, 16 + 16): round-trip delay in secondi con il relativo server di stratum 1;
  
- Root Dispersion (32 bit unsigned fixed decimal, 16 + 16): errore nominale relativo alla sorgente di riferimento primario;
  
- Reference Identifier (Reference ID) (32 bit string): identifica la particolare sorgente di riferimento. In NTPv4 i valori possibili sono:
  
  - se stratum = 0: rappresenta il messaggio spedito al client;
  - se stratum = 1: è una stringa di quattro caratteri ASCII che identifica la fonte di sincronizzazione esterna;
  - se stratum > 1: indirizzo IPv4 della sorgente di sincronizzazione.
  
- Reference timestamp: ora a cui è stato impostato o corretto per l'ultima volta l'orologio locale nel formato NTP;

- Origin timestamp: ora in cui la richiesta ha lasciato il client per il server, nel formato NTP;

- Receive timestamp: ora in cui la richiesta è arrivata al server, nel formato NTP;

- Transimit timestamp: ora in cui la risposta ha lasciato il server per il client, nel formato NTP.

![Schema anatomico del pacchetto NTP.](images/NTP - Pacchetto.png)

##### Strutture dati del protocollo NTP

NTP usa 3 diversi formati per rappresentare un istante, e sono tutti dati di tipo intero salvati in modo Big Endian (bit più significativi all'inizio): *NTP Date*, *NTP Timestamp* e *NTP Short*.

###### NTP Date

Questo formato è lungo 128 bit, è il più preciso ed è così composto:

- i bit da 0 a 31 (32 bit) indicano l'**era attuale**;
- i bit da 32 a 63 (32 bit) indicano il secondo attuale** (o **era offset**) nell'era precedentemente indicata;
- i bit da 64 a 127 (64 bit) indicano la **frazione di secondo attuale** (o **fraction**) con una precisione di $0,5 \times 10^{-18}$.

NTP internamente utilizza questo formato per i suoi calcoli.

###### NTP timestamp

Questo formato è lungo 64 bit ed è così composto:

- i bit da 0 a 31 (32 bit) indicano il **secondo attuale**. Si suppone che il ricevente conosca l'era attuale;
- i bit da 32 a 63 (32 bit) indicano la **frazione di secondo attuale** con una precisione dimezzata rispetto a quella fornita da NTP date.

Anche se meno preciso le comunicazioni solitamente utilizzano questo formato perché rappresenta il compromesso migliore tra ritardi di trasmissione e precisione. Per questo motivo **la sincronizzazione avviene con successo solo se gli orologi da sincronizzare non discostano più di metà era**, ovvero 68 anni.

Per assegnare ad una macchina un valore di orologio non troppo discostato da quello reale si ricorre a fonti esterne, come l'orologio da taschino del sistemista. :smile:

###### NTP short

Questo formato è lungo 32 bit e è così composto:

- i bit da 0 a 15 (16 bit) indicano il **secondo attuale**;
- i bit da 16 a 31 (16 bit) indicano la **frazione di secondo attuale**.

#### Client NTP per sistemi Linux

##### ntpdate

Il comando ntpdate deve operare come root ed è un po' grezzo, si collega ad uno o più server NTP passati come argomenti e li interroga mediante il protocollo NTP per conoscere l'offset del proprio clock rispetto al server. Il comando applica alcuni degli algoritmi di selezione e filtering dei campioni ricevuti e determina il migliore.

Dopo avere individuato l'aggiustamento da effettuare, il comando ntpdate setta l'orologio in due modi diversi:

- Se l'errore è maggiore di 0.5 secondi, ntpdate setta direttamente l'orologio invocando la funzione `settimeofday()`;
- se invece l'errore è minore di 0.5 secondi, ntpdate usa la funzione adjtime che modifica gradatamente la velocità del clock.

##### ntpd

"ntpd" è un demone per l'aggiornamento automatico dell'orologio.

Fornisce le seguenti utility:

- **ntpq**: serve per interrogare il demone locale di NTP. Lanciando semplicemente il comando `ntpq -p` viene visualizzato lo stato dei server attualmente utilizzati dal demone ntpd locale;
- **ntpstat**: visualizza informazioni sullo stato di sincronizzazione del tempo fornito da NTP.

## Orologi in contesti virtualizzati

Il clock hardware può essere:

- una versione mutuata del clock hardware, manutenuto da clock software del s.o. fisico;
- potrebbe essere virtualizzato, quindi la macchina virtuale non vedrà mai quello hw reale, evitando così il propagarsi di problemi.



