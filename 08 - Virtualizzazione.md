# Virtualizzazione

Virtualizzare significa rappresentare virtualmente, ovvero attraverso software, qualcosa: un intero computer, un suo singolo componente, una rete.

Esistono vari **tipi di virtualizzazione**:

- virtualizzazione completa o nativa: virtualizzazione che crea un ambiente isolato e protetto in cui eseguire un'entità. Tale entità deve poter essere eseguibile sul dispositivo sul quale poggia il sistema di virtualizzazione senza alcuna traduzione di sorta;
- emulazione: virtualizzazione che crea un livello hardware astratto per l'esecuzione di un'entità non compatibile con quella di supporto al sistema di virtualizzazione. Il processo di emulazione risulta più lento rispetto alle due forme di virtualizzazione precedenti, a  causa della traduzione delle istruzioni dal formato del sistema ospite a quello del sistema ospitante.

Le entità della virtualizzazione possono essere sistemi operativi od applicazioni e sono divisibili in:

- **host**: entità ospitante su cui risiede il sistema di virtualizzazione;
- **guest**: entità che utilizza il sistema di virtualizzazione.

La **virtualizzazione completa**, oggetto di questo riassunto, si articola in altre due **tipologie**:

- virtualizzazione di livello hardware: il guest è un sistema operativo;
- virtualizzazione di livello sistema operativo o *containerizzazione*: il guest è un applicativo.

![Visualizzazione grafica del livello a cui operano i diversi tipi di virtualizzazione completa.](images/Virtualizzazione - Stack tecnologie.svg)

## Spazio kernel e spazio utente

Le istruzioni macchina eseguite da una CPU si distinguono in:

- istruzioni privilegiate: eseguibili solo dal kernel del sistema operativo installato sulla macchina fisica, entrando nell'apposito ring di esecuzione;
- istruzioni generali: eseguibili anche dallo spazio utente.

![Spazio kernel e spazio utente.](images/Virtualizzazione - Spazio kernel ed utente.png)

## Virtualizzazione di livello hardware

La virtualizzazione di livello hardware si occupa di creare un ambiente isolato e protetto in cui eseguire un sistema operativo guest, con l'obiettivo di:

- circoscrivere l'esecuzione dei sistemi operativi guest, sia tra loro che rispetto al sistema operativo host;
- eseguire simultaneamente più sistemi operativi in una stessa macchina fisica;

Per fare ciò l'**HyperVisor (HV)**, software dedito a gestire il livello di astrazione, fornisce un hardware virtuale che ha la peculiarità di rendere disponibile il maggior numero funzionalità hardware, a partire dal set di istruzioni della macchina ospitante, quindi il guest deve potenzialmente poter essere eseguito direttamente sull'hardware (altrimenti sarebbe emulazione). Ogni guest avrà una sua istanza separata di tale hardware virtuale; tale livello permette al guest di essere eseguito su host che espongono le stesse funzionalità hardware (set di istruzioni, ...) ma che differiscono per configurazione hardware specifica.

![Stack delle entità coinvolte nella virtualizzazione di livello hardware.](images/Virtualizzazione HW - Stack entità.png)

Esistono due tipo di HV:

- tipo 1 o **bare-metal**: l'HV copre anche il ruolo di host;
- tipo 2 o **hosted** o full-virtualization: l'HV è un applicativo di un sistema operativo host.

Ovviamente gli HV di tipo 1 permettono di avere prestazioni di virtualizzazione maggiori, visto che l'HV è un software specific-purpose, a discapito di non poter utilizzare la macchina fisica con un sistema operativo e di una utilizzabilità generalmente meno confortevole (anche se dipende dalle singole soluzioni).

L'esecuzione delle syscall, ovvero le istruzioni privilegiate eseguite dal processore in ring 0, impartite dal guest sono un punto delicato in quanto i privilegi con il quale tali istruzioni sarebbero eseguite nel processore potrebbero ignorare il livello di isolamento creato dall'HV nello userspace. Tale problematica è affrontata in modi differenti:

- attraverso la **para-virtualization** il guest ha coscenza di essere eseguito in un ambiente virtualizzato ed utilizza delle syscall apposite, le *hypercall*, che utilizzano l'hardware virtualizzato piuttosto che quello della macchina fisica. Le modifiche al sistema operativo sono generalmente fornite dallo sviluppatore del kernel, visto che richiedono che si conosca bene il kernel sul quale il sistema operativo si basa;
- nel caso non sia disponibile la para-virtualization allora si parla di **full-virtualization**. Il guest non ha coscienza di essere virtualizzato e l'esecuzione delle istruzioni privilegiate da parte del guest è affrontata in due modalità differenti:
  - se è supportata la full-virtualization hardware assisted l'esecuzione delle chiamate di sistema utilizza un insiemi di tecnologie, messi a disposizione dalla CPU stessa, che forniscono le stesse funzionalità dello 0 in un ambiente isolato. Le tecnologie in questione sono le Intel VT-x per i processori Intel e le AMD-V per i processori AMD;
  - altrimenti l'HV esegue la traduzione binaria (binary translation) delle istruzioni. Tale tecnica consiste nella sostituzione del codice macchina delle syscall del sistema guest con un'implementazone che accede alle risorse virtuali, invece di quelle fisiche della macchina. Tale soluzione è stata inizialmente adottata dall'HV VMWare ed è la meno prestante.

In un'ottica di **sicurezza** l'host è schermato dalle falle del guest, ma non viceversa, cosi come non sono schermate tutte le risorse a cui il guest ha accesso diretto.

## Virtualizzazione di livello sistema operativo

Tecnologia di virtualizzazione volta a virtualizzare l'ambiente di esecuzione dei singoli applicativi. Questa permette di condividere le dipendenze comuni a più applicazioni e gestire il caso in cui all'interno di uno stesso sistema operativo più applicazioni di una stessa macchina virtuale necessitano di diverse versioni delle dipendenze condivise, senza impiegare tutte le risorse necessarie a virtualizzare un intero sistema operativo.

L'idea di base è quella di creare molteplici user space isolati l'uno dall'altro, chiamati **container**, tramite funzionalità apposite messe a disposizione dal kernel dell'host. Ognuno di questi avrà alcune risorse ad esso riservate ed altre condivise, in base alle scelte dell'amministratore di tali container. L'unico componente sempre condiviso tra tutti i container che eseguono su una stessa macchina è il kernel.

Essendo tale tecnologia di virtualizzazione dipendente dal sistema operativo il contenuto di un container può essere eseguito soltanto in container eseguiti su kernel con una syscall interface congruente; in altre parole il contenuto di un container è kernel-dependent, ed occorrerà fornire più versioni in base al kernel sul quale il container sarà creato.

==È possibile condividere risorse tra container, ma non lo abbiamo mai affrontato a lezione==

![Virtualizzazione SO - Stack delle entità coinvolte nella virtualizzazione di livello software.](images/Virtualizzazione SO - Stack entità.png)

I container sono creati partendo da un'**immagine**, ovvero un insieme di file eseguibile dal kernel sul quale il container risiede (esempio: userspace di una distribuzione Linux e pacchetti necessari allo scopo del container); praticamente è il duale di un disco di installazione di un sistema operativo per quanto riguarda la virtualizzazione di livello hardware.

### Le container runtime

Una container runtime è un servizio dell'host che si occupa di gestire il ciclo di vita di un container, ma non dell'esecuzione del suo contenuto. Si dividono in low-level ed high-level; solitamente ci si riferisce a quelle high-level quando si utilizza soltanto il termine "container runtime".

Le **low-level** container runtime si occupano soltanto dell'esecuzione dei container, assicurando l'isolamento di ognuno dei container. Quelle attualmente più famose sono:

- runc: sviluppato dalla Open Container Initiative (OCI) come reference implementation del suo stesso standard "runtime-spec". Inizialmente era parte di Docker;
- lxc: sviluppato da Canonical, focuses on containers as lightweight machines - basically servers that boot faster and need less RAM. 

Le **high-level** container runtime supportano anche altre funzionalità, come la gestione delle immagini e la gestione di bus per la comunicazione con il container eseguito. Quelle attualmente più famose sono:

- cri-o: sviluppata da RedHat, Intel, SUSE, Hyper e IBM, is an implementation of the Kubernetes CRI (Container Runtime Interface) to enable using OCI (Open Container Initiative) compatible runtimes. It is a lightweight alternative to using containerd or rkt as the runtime for kubernetes. It allows Kubernetes to use any OCI-compliant runtime as the container runtime for running pods. Today it supports runc and Kata Containers as the container runtimes but any OCI-conformant runtime can be plugged in principle;
- containerd: sviluppato dalla Cloud Native Computing Foundation, è il continuo del progetto "libcontainer" iniziato da Docker ed implementa anche l'interfaccia Kubernetes CRI. Si poggia su runc;
- rkt: developed by CoreOS, is a bit hard to categorize because it provides all the features that other low-level runtimes like runc provide, but also provides features typical of high-level runtimes;

![Comparativa delle funzionalità tra le runtime citate.](images/Container - Comparazione runtimes.png)

In sistemi operativi basati su Linux le container runtime sono implementate utilizzando due funzionalità del kernel: namespaces e cgroups. Namespaces let you virtualize system resources, like the file system or networking, for each container. Cgroups provide a way to limit the amount of resources like CPU and memory that each container can use. At the lowest level, container runtimes are responsible for setting up these namespaces and cgroups for containers, and then running commands inside those namespaces and cgroups. Low-level runtimes support using these operating system features.

[Source]: https://www.ianlewis.org/en/container-runtimes-part-1-introduction-container-r e successivi.

### Docker

Docker è uno dei software più utilizzati per la gestione dei container. Utilizza la container runtime containerd e fornisce una serie di **funzionalità** utili alla gestione dei container:

- portable deployment across machines: Docker defines a format for bundling an application and all its dependencies into a single object called a container. This container can be transferred to any Docker-enabled machine, with the guarantee that the execution environment exposed to the application is the same;
- automatic images build: Docker fornisce un tool chimato "build" per automatizzare la creazione di immagini con la stessa logica del programma make;
- versioning: docker includes git-like capabilities for tracking successive versions of a container, inspecting the diff between versions, committing new versions, rolling back, incremental uploads and downloads, etc;
- component re-use: è possibile utilizzare un container come parent-image per altri container, sia per container creati manualmente che tramite build ==è la stessa cosa della condivisione di librerie tra container?==;
- condivisione delle immagini: Docker fornisce un'applicazione open-source che agisce da archivio di immagini. Docker Inc. mette a disposizione un archivio pubblico, [Docker Hub](https://hub.docker.com/), della quale si occupa di garantirne la qualità dei contenuti;
- API: tutte le funzionalità di Docker sono disponibili attraverso una REST API, la stessa utilizzata dalla CLI fornita.

![Moduli che compongono Docker.](images/Docker - Moduli.png){#fig:docker_moduli}

#### Architettura

Docker è composto da:

- demone "dockerd": servizio che gestisce i container, le immagini, le interfacce di rete ed i filesystem. Espone una REST API utilizzabile da una qualsiasi socket, sia locale che remota;
- client: qualsiasi programma che utilizza uno o più demoni dockerd. Ad esempio Docker fornisce una CLI (Command Line Interface);
- registry: archivio remoto di immagini.

dockerd è il responsabile della gestione dell'archivio locale delle immagini e dei container eseguiti, nonché dell'infrastruttura necessaria ai container per comunicare tra di loro e con l'esterno.

Docker supporta anche applicazioni che per essere eseguite necessitano di più istanze della loro stessa immagine, sia per motivi di load-balancing che per scelta architetturale dell'applicativo, eseguendo ogni istanza in un diverso container e fornendo tutta una serie di funzionalità utili a gestire ed utilizzare tali istanze come se fossero un'unica entità.

![Architettura di Docker.](images/Docker - Architettura.svg)

Nella configurazione **swarm** (= sciame) molteplici istanze di dockerd sono raggruppate in modo da creare un cluster di esecuzione dei container, ove questi sono distribuiti in base a politiche di load-balancing. Le comunicazioni tra le molteplici istanze di dockerd avvengono sempre tramite REST, quindi possono essere dislocate sia in locale che in remoto.

In uno swarm ogni istanza di dockerd può ricoprire diversi ruoli:

- worker: semplice esecutore di container;
- manager: controller dei worker. Possono esistere più istanze di manager per aumentare la robustezza del cluster, ma soltanto una sarà quella attiva come controller. I manager possono anche agire come worker, in base alle disposizioni dell'amministratore.

![Architettura di un Docker swarm.](images/Docker - Swarm.jpg)

La **CLI** fornita da Docker permette di controllare sia una singola istanza di dockerd che uno swarm. Segue la seguente sintassi:

```
docker [OPTIONS] COMMAND
```

I vari comandi che mette a disposizione sono affrontati nei successivi capitoli.

#### I container

Un **container** può essere in uno dei seguenti **stati**: created, restarting, running, removing, paused, exited, or dead.

==come è possibile condividere librerie tra container?==

In Docker un'applicazione per cui si definisce un deploy tramite container è definita "applicazione Dockerizzata".

Per la gestione dei container la CLI di Docker fornisce i seguenti comandi:

- `run [OPTIONS] IMAGE [COMMAND] [ARG...]`: crea un container a partire da un'immagine ed esegue il comando COMMAND, se specificato, altrimenti il comando di default specificato nell'immagine. Se nell'archivio locale delle immagini non c'è l'immagine richiesta la scarica dai registry remoti configurati;

  - `--name [NAME]`: indica il nome del container;
  - `-t, --tty`: crea una pseudo-TTY per il container;
  - `-i, --interactive`: keep STDIN open even if not attached;
  - `-a, --attach [STDIN|STDOUT|STDERR]`: connettiti ad STDIN, STDOUT e/o STDERR;
  - `-d, --detach`: esegui il container in background;
  - `-e, --env 'NAME=VALUE'`: definisco una variabile d'ambiente di nome NAME e valore VALUE all'interno del container;
  - `--rm`: elimina automaticamente il container quando il container va in stato EXITED (da solo o con `stop` non fa differenza);

    Esempio: avviare un nuovo container con un l'immagine "ubuntu", creare un nuovo container con nome random, avviarlo (supponendo che sia configurato per lanciare bash di default) e collegare i suoi canali standard al terminale da cui è stato avviato.

    ```
    docker run -it ubuntu
    ```

    Esempio: avviare un nuovo container con l'immagine "ubuntu", il cui comando di default è bash, in modalità detached ma senza che termini, visto che se bash non ha uno STDIN attaccato termina.

    ```
    docker run -di ubuntu
    ```
  
  - `-v, --volume LOCAL_ABS_PATH:CONTAINER_ABS_PATH`: monta una cartella dell'host in una cartella del filesystem del container. Se una delle due cartelle non è presente da errore;
  
  - `--network DRIVER`: indica il driver di rete da utilizzare;
  - `--hostname NAME`: indica il nome di rete del container, ad esempio utilizzato dal servizio DNS di Docker. Di default Docker utilizza lo stesso nome del container;
  - `--ip ADDRESS`: indirizzo del container. Bypassa l'assegnazione automatica;
  - `-p, --publish [HOST_PORT:]CONTAINER_PORT[/(tcp|udp)]`: pubblica una porta del container CONTAINER_PORT su una HOST_PORT. Se HOST_PORT non è specificata ne utilizza una scelta casualmente;
  - `-P, --publish-all`: pubblica tutte le porte esposte dal container in porte random;
  
    Esempio: eseguo un container che esegue un web server, pubblico la sua porta 80 sulla porta di loopback dell'host 4080 e gli faccio utilizzare i file del sito web residenti in una cartella dell'host.

    ```shell
    ~> docker run --name nginx -v /home/vic/htdocs:/usr/share/nginx/html -p 4080:80 -d nginx
    ~> docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                  NAMES
fbc5a17edfdb        nginx               "nginx -g 'daemon of…"   21 seconds ago      Up 20 seconds       0.0.0.0:4080->80/tcp   nginx
    ```

- `start [OPTIONS] CONTAINER [CONTAINER...]`: avvia un container;

  - `-a, --attach`: attach STDOUT/STDERR and forward signals;
  - `-i, --interactive`: attach container's STDIN;

- `stop [OPTIONS] CONTAINER [CONTAINER...]`: termina l'esecuzione del container, inviando un segnale SIGTERM al comando in esecuzione sul container. Se il comando in esecuzione nel container è una shell e richiamo il comando "exit" succede la stessa cosa;

- `exec [OPTIONS] CONTAINER COMMAND [ARG...]`: esegue un comando in un container in esecuzione.

  - `-w, --workdir STRING`: working directory del comando;
  - `-u, --user NAME|UID[GROUP|GID)`: utente con il quale eseguire il comando;
  - `-t, --tty`: crea una pseudo-TTY per il comando;
  - `-i, --interactive`: keep STDIN open even if not attached;
  - `-d, --detach`: esegui il comando in background;

- `attach [OPTIONS] CONTAINER`: collegati al STDIN, STDOUT e STDERR di un container in esecuzione.

  Mentre si è attaccati ad un container è possibile mandare dei comandi a Docker componendo determinate keystrokes dopo aver immesso la sequenza di escape `CTRL`+`P`. I comandi che possono essere richiamati sono:

  - `CTRL`+`Q`: scollega STDIN, STDOUT e STERR;

- `ps [OPTIONS]`: mostra i container in esecuzione, se non altrimenti specificato. Nell'output la colonna "COMMAND" mostra il comando attualmente in esecuzione nel container;
  
- `-a, --all`: mostra anche i container non in esecuzione;
  
- `inspect [OPTIONS] NAME|ID [NAME|ID...]`: mostra informazioni di basso livello su un container;

- `rm [OPTIONS] CONTAINER [CONTAINER...]`: elimina dei container;

In Docker le **immagini** di container...

- ==è possibile definire nome e tag. tag tipo identificatore commit==
- ==non salva solo la diff, sono proprio 2 img differenti (da verificare meglio)==

Comandi riguardo le immagini:

- `images [OPTIONS] [REPOSITORY[:TAG]]`: mostra le immagini nell'archivio locale;

- `pull [OPTIONS] NAME[:TAG|@DIGEST]`: scarica un'immagine da un registry;

- `rmi [OPTIONS] IMAGE [IMAGE...]`: rimuove del le immagini dall'archivio locale;

- `search [OPTIONS] TERM`: cerca un'immagine su Docker Hub. Nel risultato mostrato la colonna "OFFICIAL" indica che l'immagine è gestita dalla Docker Inc. ==e la colonna "AUTOMATED" indica?==;

- `commit [OPTIONS] CONTAINER [REPOSITORY[:TAG]]`: salva localmente un'immagine dello stato corrente del container;
  - `-m MESSAGE`: messaggio;
  - `-a AUTHOR`: autore del commit;
- `tag`

#### Reti tra container

Docker permette di definire delle reti virtuali con il quale mettere in comunicazione i container, sia tra di loro che con l'host (quindi l'esterno). Un container può essere collegato a più reti.

Le tipologie di reti sono definibili attraverso dei plugin, chiamati "driver", che possono essere creati anche da terze parti ed aggiunti a Docker. I **driver di rete di default** sono:

- bridge (default): rete che consente ai container di comunicare tra di loro ed uscire, tramite NAT, sulla rete a cui l'host è collegato. Si possono definire più reti bridge, che saranno isolate tra di loro.
- host: il container utilizza direttamente l'interfaccia di rete al quale l'host è collegato. Dall'esterno dell'host sarà visibile un'unica interfaccia di rete, alla quale corrisponderanno due indirizzi IP differenti;
- overlay: rete di overlay. Utilizzata nella modalità swarm oppure per mettere in contatto specifici container eseguiti anche su istanze diverse di dockerd;
- macvlan: il container avrà una sua interfaccia di rete virtuale dedicata, con un suo MAC address. Dall'esterno l'host avrà più interfacce di rete che utilizzano lo stesso socket fisico;
- null: rete isolata dalla rete dell'host, utile solamente a mettere in collegamento tra loro dei container.

Docker mette a disposizione alcune reti di default, identificate dai seguenti nomi:

- bridge: rete con driver bridge al quale sono collegati tutti i container a meno che sia diversamente specificato;
- host: rete con driver host;
- none: rete con driver null.

Si possono definire delle **reti personalizzate** specificando:

- il driver;
- il nome;
- lo spazio di indirizzi che utilizza;
- il gateway di default;

Nelle reti è possibile definire anche delle address reservation e relativo nome DNS.

Per ogni rete virtuale Docker fornisce vari **servizi di rete**, quali:

- DHCP: per assegnare gli indirizzi;
- routing: si occupa di instradare i pacchetti verso l'interfaccia corretta;
- DNS: il servizio eredita la configurazione dell'host e, all'interno rete virtuale in utilizzo, mette a disposizione i nomi dei container che usano quella stessa rete, cosicché possano raggiungersi a vicenda;
- altri servizi specifici per il tipo di driver scelto.

In sistemi operativi **Linux** le reti virtuali di Docker sono implementate definendo in iptables nuove tabelle e relative regole, così da poter gestire i pacchetti in maniera separata rispetto a quelli dell'host. Tutto ciò è realizzato sempre grazie ai container runtime.

##### Reti bridge

Una **rete bridge** è una rete che consente ai container di comunicare tra di loro e con l'host tramite NAT, cosi come sulle reti al quale lui è connesso come un applicativo qualsiasi dell'host. Per realizzare questa rete Docker crea un'interfaccia di rete virtuale per ogni container della rete ed una per l'host, tutte accomunate da uno stesso spazio di indirizzi scelto automaticamente in modo tale da non coincidere con gli altri al quale l'host partecipa. Quindi le richieste verso 

In una rete bridge è possibile effettuare il **publish delle porte** dei container, ovvero mappare una porta locale ad un'interfaccia dell'host ad una porta esposta da un container. Si può specificare l'interfaccia host sul quale sarà visibile la porta pubblicata specificandone l'IP, ovvero "l'indirizzo di bind"; se non specificato sarà utilizzato l'indirizzo 0.0.0.0, quindi la porta pubblicata sarà raggiungibile da tutte le interfacce di rete dell'host.

La rete bridge predefinita, ovvero la rete con nome "bridge", di default ha le seguenti opzioni attive:
- IP masquerading;
- bind delle porte pubblicate sull'indirizzo 0.0.0.0;

In sistemi operativi Linux l'host può interfacciarsi a tale rete attraverso l'interfaccia virtuale "docker0".

```shell
~> ip address show dev docker0
5: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    link/ether 02:42:70:45:e3:c8 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever

# Notare che l'host ha l'IP del gateway di tale rete
~> docker network inspect bridge | grep Gateway
                    "Gateway": "172.17.0.1"
# IP masquerading attivo
~> docker network inspect bridge | grep masq
            "com.docker.network.bridge.enable_ip_masquerade": "true",
# IP di bind
~> docker network inspect bridge | grep bind
            "com.docker.network.bridge.host_binding_ipv4": "0.0.0.0",
```

>  Anatomia della rete docker0: https://developer.ibm.com/recipes/tutorials/networking-your-docker-containers-using-docker0-bridge/.

##### CLI

I **comandi** che mette a disposizione la CLI di Docker sono tutti sottocomandi del comando `network` (ovvero ogni comando sarà preceduto da `docker network`):

- `create [OPTIONS] NETWORK`: crea una rete. Di default utilizza il driver bridge;

  - `-d, --driver DRIVER`: utilizza uno specifico driver;
  - `--subnet CIDR_ADDRESS_SPACE`: indica, in notazione [CIDR](https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing#CIDR_notation) lo spazio di indirizzi che la rete deve utilizzare;
  - `--gateway ADDRESS`: indirizzo del gateway;
  - `--aux-address NAME=ADDRESS`: indica un indirizzo fisso nella sottorete e relativo nome DNS;
  - `-o, --opt NAME=VALUE`: imposta il valore di un'opzione driver-specific;

  > A lezione il professore fa riferimento ai seguenti parametri speciali per le reti specificabili soltanto se la rete creata utilizza il driver bridge, ma all'atto pratico non sono più disponibili:
  >
  > - `--ip-masq`: abilita l'IP masquerading per la network;
  > - `--ip`: indirizzo di binding delle porte pubblicate;
  >
  > Sono comunque settabili con il parametro `--opt`.
  
  - i parametri specifici per le reti bridge sono:
    - indirizzo di bind: com.docker.network.bridge.host_binding_ipv4;
    - IP masquerading attivo: com.docker.network.bridge.enable_ip_masquerade;

- `inspect [OPTIONS] NETWORK [NETWORK...]`: visualizza i dettagli di una rete;

- `connect [OPTIONS] NETWORK CONTAINER`: connette un container ad una rete;
  
- `--ip ADDRESS`: indirizzo del container. Bypassa l'assegnazione automatica;
  
- `disconnect [OPTIONS] NETWORK CONTAINER`: stacco un container da una rete;

- `rm`: elimina una rete;

> **Note sulla sintassi**
> - NETWORK indica che può essere utilizzato sia l'ID di una rete che il suo nome;
> - CONTAINER indica che può essere utilizzato sia l'ID di un container che il suo nome.

##### Esempio di configurazione di rete

In questo esempio si affronterà il collegamento di alcuni container tramite una topologia di rete particolare.

I container da collegare sono "container1", "container2", "container3" ed eseguiranno tutti l'immagine di "busybox" disponibile su Docker Hub [^[BusyBox](https://www.busybox.net/) è un pacchetto che fornisce una shell (nello specifico "sh") e dei tool di base per sistemi operativi Linux che occupa poco spazio; la dimensione si attesta sotto ai 5 MiB.] e saranno collegati come segue (fare riferimento alla @fig:docker_topologia_esempio_networking):

- container1 e container2 in una rete con driver bridge chiamata "bridge", ovvero quella di default a cui sono attaccati i container se non si specifica una rete;
- container2 e container3 in una rete con driver bridge chiamata "isolated_nw", con spazio di indirizzi 172.25.0.0/16;
- container2 farà da proxy per le comunicazioni tra la rete "bridge" e la rete "isolated_nw".

Saranno quindi presenti le seguenti interfacce virtuali:

- 3 interfacce virtuali per la rete "bridge": una per l'host, una per container1 ed una per container;
- 2 interfacce virtuali per la rete "isolated_nw": una per container2 ed una per container3. L'interfaccia virtuale di container3 avrà indirizzo IP fisso 172.25.3.3.

container2 quindi avrà quindi due interfacce di rete.

![Topologia della rete implementata dell'esempio.](images/Docker - Topologia esempio networking.png){#fig:docker_topologia_esempio_networking}

Questi sono i passi per riprodurre l'esempio:

1. Creare ed eseguire in background i container container1 e container2:

   ```shell
   ~> docker run --detach --interactive --tty --name container1 busybox
   ~> docker run --detach --interactive --tty --name container2 busybox
   ~> docker ps
   CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
   eb7dd15549d1        busybox             "sh"                6 minutes ago       Up 3 minutes                            container2
   1bfe28c416a8        busybox             "sh"                2 hours ago         Up 2 hours                              container1
   ```

   container1 e container2 saranno collegati alla rete "bridge", 

   Sarà possibile collegarsi alle shell dei due container con il comando `docker attach NOME_CONTAINER`.

2. Creo la rete "isolated_nw":

   ```shell
   ~> docker network list
   NETWORK ID          NAME                DRIVER              SCOPE
   3249f0fc1892        bridge              bridge              local
   d759ec659ecf        host                host                local
   ec93044a7165        none                null                local
   ~> docker network create --driver bridge --subnet 172.25.0.0/16 isolated_nw
   ~> docker network list
   NETWORK ID          NAME                DRIVER              SCOPE
   3249f0fc1892        bridge              bridge              local
   d759ec659ecf        host                host                local
   698fe40932da        isolated_nw         bridge              local
   ec93044a7165        none                null                local
   ```

   Nell'environment utilizzato per la stesura dell'esempio la rete "bridge" ha spazio di indirizzi 172.17.0.0/16.

   ```shell
   ~> docker inspect bridge | grep Subnet
                      "Subnet": "172.17.0.0/16",
   ```

3. Collegare container2 alla rete "isolated_nw":

    ```shell
    ~> docker attach container2
    / # ip addr
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
        inet 127.0.0.1/8 scope host lo
           valid_lft forever preferred_lft forever
    30: eth0@if31: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue 
        link/ether 02:42:ac:11:00:03 brd ff:ff:ff:ff:ff:ff
        inet 172.17.0.3/16 brd 172.17.255.255 scope global eth0
           valid_lft forever preferred_lft forever
    / # read escape sequence
    ~> docker network connect isolated_nw container2
    ~> docker attach container2
    / # ip addr
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
        inet 127.0.0.1/8 scope host lo
           valid_lft forever preferred_lft forever
    30: eth0@if31: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue 
        link/ether 02:42:ac:11:00:03 brd ff:ff:ff:ff:ff:ff
        inet 172.17.0.3/16 brd 172.17.255.255 scope global eth0
           valid_lft forever preferred_lft forever
    32: eth1@if33: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue 
        link/ether 02:42:ac:19:00:02 brd ff:ff:ff:ff:ff:ff
        inet 172.25.0.2/16 brd 172.25.255.255 scope global eth1
           valid_lft forever preferred_lft forever
    / # ip route
    default via 172.17.0.1 dev eth0
    172.17.0.0/16 dev eth0 scope link  src 172.17.0.3
    172.25.0.0/16 dev eth1 scope link  src 172.25.0.2
    / # read escape sequence
    ```

4. Creare ed eseguire container3:

   ```shell
   ~> docker run --detach --interactive --tty --name container3 --network isolated_nw --ip 172.25.3.3 busybox
   ~> docker attach container3
   / # ip addr
   1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue qlen 1000
       link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
       inet 127.0.0.1/8 scope host lo
          valid_lft forever preferred_lft forever
   36: eth0@if37: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue 
       link/ether 02:42:ac:19:03:03 brd ff:ff:ff:ff:ff:ff
       inet 172.25.3.3/16 brd 172.25.255.255 scope global eth0
          valid_lft forever preferred_lft forever
   / # read escape sequence
   ~> docker inspect network isolated_nw 
   [...]
           "Containers": {
               "06bc0fa6ab7f9bcb8bbe92e63d612116d0edc162c003123e87e9aa9d4e368c27": {
                   "Name": "container3",
                   "EndpointID": "b8218166bb28e7ac2f1ef2297b7ea29f0653fffc38820808ab91ff30b81bf9e8",
                   "MacAddress": "02:42:ac:19:03:03",
                   "IPv4Address": "172.25.3.3/16",
                   "IPv6Address": ""
               },
               "eb7dd15549d1e7958d890f1c6f7c4e5ff84407910d2ecaf09c7b1a4d7dacc78c": {
                   "Name": "container2",
                   "EndpointID": "ba58a939f6afd60d74ed8d9c8fac5e2f75e81cd1e84d116afe3bfc11cb83f071",
                   "MacAddress": "02:42:ac:19:00:02",
                   "IPv4Address": "172.25.0.2/16",
                   "IPv6Address": ""
               }
           },
   [...]
   ```

   Si può notare che container3 è collegato alla rete isolated_nw.

5. Configurazione di container2 come proxy tra la rete bridge e la rete isolated_nw.

   - vedere il file ["Linux as Proxy.txt"](files/Linux as Proxy.txt).

#### Dockerfile

Il modulo "build" di Docker, visibile nella @fig:docker_moduli, permette di automatizzare le operazioni che di costruzione di un container. La creazione dell'immagine avviene tramite una sedie di comandi, descritti in un file chiamato "Dockerfile".

I comandi specificabili in un Dockerfile sono:

- `FROM`: indica l'immagine di partenza. Se specificato deve essere il primo comando specificato nel Dockerfile;
- `MANTAINER`: metadati riguardo l'autore dell'immagine. Non fa veramente qualcosa, è soltanto un metadato;
- `RUN`: esegue un comando all'interno del container;
- `COPY <FILE>`: copia un file dell'host all'interno del container;
- `ADD <URL>`: copia un file raggiungibile all'URL specificato all'interno del container;
- `ENV`: definisce una variabile d'ambiente all'interno del container, che sarà visibile sia durante il processo di build che nell'immagine, quindi durante l'esecuzione del container;
- `CMD|ENTRYPOINT`: comando di default dell'immagine;
- `WORKDIR`: specifica la directory di default dell'immagine;
- `USER`: specifica l'utente predefinito dell'immagine;
- `VOLUME`: specifica un mount tra una directory dell'host ed una directory del container;
- `EXPOSE`: espone una porta del container. Non fa veramente qualcosa, è soltanto un metadato;

==impostazioni go per user-defined bridge network==

Per trattare un Dockerfile dalla CLI di docker sono disponibili i seguenti comandi:

- `build [OPTIONS] PATH|URL|-`: build an image from a Dockerfile, che può essere fornito come path locale, URL o in STDIN. Se la path indica una directory lui cerca un file chiamato "Dockerfile";
  - `-t, --tag NAME:TAG`: definisce il nome e la tag dell'immagine. L'immagine con lo stesso nome viene rimpiazzata;

#### Docker Compose e Docker Swarm

Docker Compose e Docker Swarm sono due tool, sviluppati da Docker Inc., che si occupano di automatizzare la costruzione e il dispiegamento dei container necessari ad eseguire un insieme di servizi. Tali servizi possono essere o meno correlati tra loro, ma l'utilizzo più tipico è quello di automatizzare l'esecuzione dei container di un'applicazione a microservizi, che quindi è composta da più servizi.

Ad ogni servizio corrispondono un'immagine ed un comando di tale immagine da eseguire di default quando si crea un container, che può essere differente dal comando di default dell'immagine. Docker Compose e Docker Swarm permettono di eseguire più container corrispondenti alla stessa immagine di servizio, sia per motivi di load-balancing che per design architetturale del servizio.

Entrambi i tool includono un orchestratore.

##### Compose file

Docker Compose e Docker Swarm possono costruire e dispiegare servizi in base alle istruzioni descritte su un file YAML, chiamato "Compose file". Questo descrive i servizi da creare e vari altri aspetti relativi ad essi, come le reti che li collegano, filesystem condivisi o informazioni specifiche di cui devono disporre.

Le specifiche del file sono descritte nella documentazione al link [compose file reference](https://docs.docker.com/compose/compose-file/). In questa sede sarà presa come riferimento l'ultima versione disponibile al momento della scrittura, ovvero la 3.8.

Le principali top-level entry del compose file sono:

- version: versione di specifica utilizzata dal file. Se non è indicata si assume che sia la 1.0;
- services: servizi dell'applicazione;
- networks: networking set-up;
- volumes: filesystems;
- secrets (solo Docker Swarm): way in which you applications can pull secret informations like passwords;
- configs: external configuration files of your containers. Keeping configurations external will make your containers more generic

L'entry **services** si presenta con questa sintassi:

```yaml
services:
	NOME_SERVIZIO_1:
		[ATTRIBUTI...]
	NOME_SERVIZIO_2:
		[ATTRIBUTI...]
	[...]
```

Di ogni servizio si possono specificare vari attributi, tra i quali:

- `build: ATTRIBUTES`: configuration options that are applied at images build time. Di questo attributo occorre definire obbligatoriamente la directory al quale si trova il dockerfile o l'immagine di partenza. Alcuni attributi:
  - `context: STRING` (obbligatorio se non è definito l'attributo `image`): directory nel quale si trova il dockerfile;
  - `image: IMAGE` (obbligatorio se non è definito l'attributo `context`): immagine di partenza;
  - `dockerfile`: path al dockerfile;
- `links: SERVICES_NAME_LIST`: servizi che si devono trovare nella stessa rete virtuale del servizio descritto, in modo tale che possano interloquire;
- `ports: PORT_MAPPINGS_LIST`: porte esposte dal servizio che devono essere pubblicate;
- `entrypoint: COMMAND|COMMAND_LIST`: sovrascrive l'entry point di default dell'immagine del servizio;
- `container_name: NAME` (solo Docker Compose): indica un nome per il container relativo al servizio, nel caso il servizio ne richieda soltanto uno;
- `environment: VARIABLES_LIST`: variabili d'ambiente disponibili all'interno dei container del servizio;

##### Docker Compose

Docker Compose permette di dispiegare applicazioni a microservizi su di un'unica istanza di dockerd.  Non è fornito di default dall'installazione di Docker.

###### CLI

I comandi di Docker Compose seguono questa sintassi:

```
docker-compose [-f <arg>...] [options] [COMMAND] [ARGS...]
```

- `-f`: serve a specificare il file YAML da cui ricavare le direttive. Se non si specifica niente cercherà il file `docker-compose.yaml` nella directory corrente. Essendo un'opzione globale si intuisce che ogni comando di Docker Compose necessita di leggere il file YAML;

I comandi messi a disposizione sono:

- `build`: build or rebuild services, ovvero crea le immagini per i container dei servizi. La procedura di build produce varie immagini intermedie, che sono salvate nella cache di Docker Compose per rebuild futuri;
  - `--no-cache`: non utilizzare le immagini parziali salvate in cache;
- `up [options] [--scale SERVICE=NUM...] [SERVICE...]`: builds, (re)creates, starts, and attaches to containers for a service;
  - `-d, --detach`: run containers in detached mode and print new container names;
  - `--build`: re-build images if the Dockerfile or the images have changed;
- `ps`: mostra i container relativi ai servizi;
- `exec SERVICE_NAME COMMAND_NAME COMMAND_ARGUMENTS`: esegue un comando nei container del servizio con nome SERVICE_NAME;
- `run SERVICE_NAME COMMAND_NAME COMMAND_ARGUMENTS`: esegue un comando in un nuovo container dell'immagine del servizio con nome SERVICE_NAME;
  - `-p`: pubblica una porta del nuovo container creato;
- `logs SERVICE_NAME`: logs dei container del servizio con nome SERVICE_NAME;
- `top`: processi nei container relativi ai servizi;
- `stop`: termina l'esecuzione dei container eseguiti da Docker Compose;
- `down`: come `stop` ma elimina anche le reti condivise, i volumi ed i container dei servizi.

###### Esempio con applicativo WEB

Si vuole utilizzare Docker Compose per automatizzare il dispiegamento di un applicativo web.

L'applicativo è composto da:

- un database [Redis](https://redis.io/);
- [Flask](https://flask.palletsprojects.com): a lightweight WSGI web application framework. [WSGI](https://wsgi.readthedocs.io/) è uno standard Python that describes how a web server communicates with web applications, and how web applications can be chained together to process one request;
- un server web in Python che, ad ogni richiesta, incrementa un contatore salvato nel database e restituisce una pagina web che mostra il valore del contatore dopo l'incremento.

Questo è il codice del server web in Python (file "webapp.py"):

```python
from flask import Flask
from redis import Redis

app = Flask(__name__)
redis = Redis(host="redis")

@app.route("/")
def hello():
	visits = redis.incr('counter')
	html = "<p>Visits: {visits}</p>"
	return html.format(visits=visits)

if __name__ == "__main__":
	app.run(host="0.0.0.0", port="80")
```

Dal codice si evince che:

- l'oggetto "redis" (riga 5), che rappresenta l'istanza del server Redis, cercherà di instaurare una connessione con il server Redis all'host con indirizzo "redis";
- il server web è raggiungibile alla porta 80 di qualsiasi interfaccia dell'host in cui è eseguito. Quando arriverà una richiesta sarà eseguita l'applicazione Flask dichiarata alla variabile "app" (riga 14);

Considerate le dipendenze tra le componenti si individuano i seguenti microservizi, ovvero insiemi di componenti che possono avere vita propria. Ogni microservizio può essere allocato in un diverso container, in modo tale da poter successivamente distribuire i container come meglio si crede; l'unico requisito è che i microservizi colloquino attraverso protocolli di rete, così da poterli mettere in contatto attraverso le reti virtuali di Docker. Seguono i Dockerfile dei microservizi individuati:

- Dockerfile del server web (webapp.dockerfile):

  ```dockerfile
  FROM python:3.7
  WORKDIR /app
  RUN pip install Flask Redis
  ADD webapp.py /app/webapp.py
  EXPOSE 80
  CMD ["python", "webapp.py"]
  ```
  
- il Dockerfile del database Redis non è specificato, è sufficiente l'immagine fornita da Docker Hub.

Quindi il file YAML di Docker Compose dell'intero applicativo sarà così composto (docker-compose.yaml):

```yaml
version: "3.8"

services:

  app:
    build:
      context: .
      dockerfile: webapp.dockerfile
    links:
      - db
    ports:
      - "8888:80"

  db:
    image: redis
```

In questo file possiamo notare la descrizione di due servizi:

- redis: corrisponde semplicemente all'immagine di nome "redis" (che ci aspettiamo contenga un server Redis) disponibile nel repository locale, quindi non occorre personalizzare niente;
- server: l'immagine del nostro server web sarà creata seguendo le istruzioni contenute nel file webapp.dockerfile. All'esecuzione Docker dovrà pubblicare la porta 80 del container sulla porta 8888 dell'host; in questo modo non occorrerà sapere l'indirizzo del container per poterlo contattare, utile soprattutto nel caso il servizio sia implementato da più container.

Procediamo all'implementazione dei servizi:

1. creiamo le immagini dei container dei servizi:

    ```shell
    ~/WebApp> docker-compose build
    db uses an image, skipping
    Building app
    Step 1/6 : FROM python:3.7
    3.7: Pulling from library/python
    376057ac6fa1: Pull complete
    5a63a0a859d8: Pull complete
    496548a8c952: Pull complete
    2adae3950d4d: Pull complete
    0ed5a9824906: Pull complete
    bb94ffe72389: Pull complete
    70d0b3967cd8: Pull complete
    5efaeecfa72a: Pull complete
    db8816f44548: Pull complete
    Digest: sha256:00759a433cfec8c420688d4d0f941bc789d84bd248cda8d8ece7c2a93554eac2
    Status: Downloaded newer image for python:3.7
     ---> 5e996c9d7c99
    Step 2/6 : WORKDIR /app
     ---> Running in 93a2726ea258
    Removing intermediate container 93a2726ea258
     ---> c3e2083f0610
    Step 3/6 : RUN pip install Flask Redis
     ---> Running in 4da3adafc50c
    Collecting Flask
      Downloading Flask-1.1.2-py2.py3-none-any.whl (94 kB)
    Collecting Redis
      Downloading redis-3.5.3-py2.py3-none-any.whl (72 kB)
    Collecting Jinja2>=2.10.1
      Downloading Jinja2-2.11.2-py2.py3-none-any.whl (125 kB)
    Collecting click>=5.1
      Downloading click-7.1.2-py2.py3-none-any.whl (82 kB)
    Collecting itsdangerous>=0.24
      Downloading itsdangerous-1.1.0-py2.py3-none-any.whl (16 kB)
    Collecting Werkzeug>=0.15
      Downloading Werkzeug-1.0.1-py2.py3-none-any.whl (298 kB)
    Collecting MarkupSafe>=0.23
      Downloading MarkupSafe-1.1.1-cp37-cp37m-manylinux1_x86_64.whl (27 kB)
    Installing collected packages: MarkupSafe, Jinja2, click, itsdangerous, Werkzeug, Flask, Redis
    Successfully installed Flask-1.1.2 Jinja2-2.11.2 MarkupSafe-1.1.1 Redis-3.5.3 Werkzeug-1.0.1 click-7.1.2 itsdangerous-1.1.0
    Removing intermediate container 4da3adafc50c
     ---> f21c1223d11e
    Step 4/6 : ADD webapp.py /app/webapp.py
     ---> affc29814366
    Step 5/6 : EXPOSE 80
     ---> Running in 6e15ae34e312
    Removing intermediate container 6e15ae34e312
     ---> baeacb935622
    Step 6/6 : CMD ["python", "webapp.py"]
     ---> Running in 190c92c6e0f8
    Removing intermediate container 190c92c6e0f8
     ---> 6dc833a11817
    
    Successfully built 6dc833a11817
    Successfully tagged webapp_app:latest
    ```

    Con la freccia `--->` sono indicate le immagini intermedie salvate nella cache di Docker Compose.

    Ora nell'archivio locale delle immagini Docker appariranno le immagini "webapp_app" e "python":

    ```shell
    ~/WebApp> docker images
    REPOSITORY          TAG                 IMAGE ID            CREATED            SIZE
    webapp_app          latest              f216f9b0a707        XXX                929MB
    python              3.7                 5e996c9d7c99        XXX                919MB
    ```

    Notare che il nome dell'immagine è composto dal nome della cartella nel quale il comando è eseguito seguito dal nome della sezione dello YAML dal quale l'immagine è stata generata. Questo indica come Docker Compose si aspetti che tutti i file di una unica applicazione risiedano in una cartella a loro dedicata, quindi è buona norma dedicargliene una.

2. eseguiamo i container precedentemente generati:

   ```shell
   ~/WebApp> docker-compose up -d
   Pulling redis (redis:)...
   latest: Pulling from library/redis
   afb6ec6fdc1c: Already exists
   608641ee4c3f: Pull complete
   668ab9e1f4bc: Pull complete
   78a12698914e: Pull complete
   d056855f4300: Pull complete
   618fdf7d0dec: Pull complete
   Digest: sha256:d27740b5bd12087efc2b30ac9102fa767d6cc83611dc0fc28f0edb042e835996
   Status: Downloaded newer image for redis:latest
   Creating webapp_redis_1 ... done
   Creating webapp_app_1   ... done
   ```
   Vediamo i container messi in esecuzione:
   
   ```shell
   ~/WebApp> docker ps -a
   CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                  NAMES
   d91ab7d0fb4d        webapp_app          "python webapp.py"       8 seconds ago       Up 7 seconds        0.0.0.0:8888->80/tcp   webapp_app_1
6b979986db3a        redis               "docker-entrypoint.s…"   9 seconds ago       Up 8 seconds        6379/tcp               webapp_redis_1
   ```
   
   E le reti create:
   
   ```shell
   ~/WebApp> docker network list
   NETWORK ID          NAME                DRIVER              SCOPE
   8ca79d9d8e0e        bridge              bridge              local
   d759ec659ecf        host                host                local
   ec93044a7165        none                null                local
   ```
   
   Non avendo specificato una particolare rete nel file YAML i container dispiegati sono stati tutti assegnati alla rete "bridge", ovvero la rete di default.
   
3. Grazie al mapping delle porte impartito con la direttiva `ports` del file docker-compose.yaml è possibile raggiungere il container all'indirizzo http://localhost:8888. Ad ogni refresh della pagina si vedrà aumentare il contatore.

##### Docker Swarm

Docker Swarm permette di effettuare le stesse operazioni di Docker Compose ma su di un cluster di dockerd, solitamente distribuito su più host. Si utilizza quando non si hanno particolari necessità nel dispiegamento dei servizi, in quanto è uno strumento molto semplice.

Il cluster di dockerd è chiamato **swarm** (= sciame), mentre le singole istanze di dockerd sono chiamati **nodi**. Un'insieme di servizi che eseguono su uno swarm è chiamato **stack**. In uno stack ogni container in esecuzione è contenuto in un task. I task relativi ad un servizio sono chiamati **replica** del servizio.

Un **task** è un'unità atomica di schedulazione in uno swarm. Il suo compito è quello di monitorare e gestire il funzionamento di un container, fornendo funzionalità utili allo swarm manager: in questo modo il container non necessita di particolari modifiche per essere eseguito in uno swarm e lo swarm può gestire un qualsiasi container.

![Servizi, task, replica e container di uno swarm.](images/Docker - Entità Swarm.png)

Il **ruolo del manager** quindi è quello di distribuire i task tra i vari worker e di distribuire le richieste ad un servizio tra le sue varie repliche, il tutto secondo logiche di load-balancing. Il manager inoltre mantiene il repository delle immagini dei container dello swarm. Le comunicazioni tra i nodi  manager e worker avvengono sulle porte 2376 e 2377.

![Architettura di uno swarm.](images/Docker - Architettura swarm.svg)

###### CLI

Swarm è un modulo di Docker, quindi è utilizzabile tramite dei comandi della Docker CLI (quindi ogni comando qui esposto deve essere preceduto da `docker`).

Comando `swarm COMMAND`: gestione di uno swarm.

- `init [OPTIONS]`: crea uno swarm;
- `join [OPTIONS] MANAGER_IP:PORT`: aggiungi il nodo corrente allo swarm gestito dal manager all'indirizzo IP MANAGER_IP
  - `--token TOKEN`

Comando `service COMMAND`: gestisce un servizio di uno swarm.

- `create [OPTIONS] IMAGE [COMMAND] [COMMAND_ARGS...]`: crea un nuovo servizio;

Comando `stack [OPTIONS] COMMAND`: gestisce l'intero stack di uno swarm.

- `deploy|up [OPTIONS] STACK`: deploy a new stack or update an existing stack.
  - `-c, --compose-file PATH`: ottiene i dettagli dello stack da un compose file;

###### Esempio con applicativo TCP

Si vuole eseguire un applicativo che risponde a delle richieste TCP sulla porta 61000 con l'identificativo dell'host sul quale è eseguito. Tale applicazione sarà eseguita in più repliche su uno swarm, quindi la risposta conterrà l'identificativo del nodo dello swarm che esegue la replica a cui il manager ha affidato la richiesta.

Lo scenario è il seguente:

- due macchine virtuali, "deb1" e "deb2", entrambi con installato Docker. Le due macchine sono collegate da una rete virtuale in cui sono visibili soltanto loro due, con spazio di indirizzi 192.168.20.0/24.
- deb1 esegue il manager dello swarm ed ha due interfacce di rete: una sulla rete virtuale per comunicare con deb2 ed un'altra che gli permette di interfacciarsi con l'host fisico. Quest'ultima ha spazio di indirizzi 192.168.10.0/24;
- un client per testare lo scenario che esegue un grande numero di thread, ognuno dei quali effettua una richiesta all'applicativo.

I file necessari sono i seguenti:

- [client.c](files/Swarm - Esempio applicativo TCP/Client/client.c) e relativo [Makefile](files/Swarm - Esempio applicativo TCP/Client/Makefile) per compilarlo;
- [server.c](files/Swarm - Esempio applicativo TCP/Server/server.c), relativo [Makefile](files/Swarm - Esempio applicativo TCP/Server/Makefile) per compilarlo, [Dockerfile](files/Swarm - Esempio applicativo TCP/Server/Dockerfile) per creare l'immagine del container e [docker-compose.yml](files/Swarm - Esempio applicativo TCP/Server/docker-compose.yml) per eseguire il servizio sullo swarm.

Nell'esempio che si va a dettagliare gli indirizzi IP delle macchine sono i seguenti:

- host: presente solo nella sottorete 192.168.10.0/24 con indirizzo 192.168.10.1;
- deb1: presente nella sottorete 192.168.10.0/24 con indirizzo 192.168.10.128 e nella sottorete 192.168.20.0/24 con indirizzo 192.168.20.128;
- deb2: presente nella sottorete 192.168.20.0/24 con indirizzo 192.168.10.129.

Creazione dello swarm:

- nella macchina deb1 creare lo swarm

  ```shell
  ~> docker swarm init --advertise-addr 192.168.20.129
  Swarm initialized: current node (dc997y1uxzaah36vkozirrqhf) is now a manager.
  
  To add a worker to this swarm, run the following command:
  
      docker swarm join --token SWMTKN-1-0ywie6vr6hg3gz51czv2i25xuv4rqf72djsv1u8b35fbctavxj-3ygrwrtyz9j7w8fgbgksu2ydc 192.168.20.129:2377
  
  To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
  ```

  deb1 crea uno swarm e fornisce all'interfaccia 192.168.20.128, ovvero quella sulla sottorete virtuale di comunicazione con deb2, una socket alla porta 2377 che permette alle altre istanze di dockerd eseguite nelle altre VM di joinare lo swarm.

- joinare l'istanza di dockerd di deb2 allo swarm:

  ```shell
  ~> sudo docker swarm join --token SWMTKN-1-0ywie6vr6hg3gz51czv2i25xuv4rqf72djsv1u8b35fbctavxj-3ygrwrtyz9j7w8fgbgksu2ydc 192.168.20.129:2377
  This node joined a swarm as a worker.
  ```

- un nodo manager può vedere la lista di worker nel suo swarm:

  ```shell
  ~$ sudo docker node list
  ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
  dc997y1uxzaah36vkozirrqhf *   deb1                Ready               Active              Leader              18.09.1
  rwdi5f79y0u2raie9l819bwka     deb2                Ready               Active                                  18.09.1
  ```

- creazione del repository dello swarm sulla porta 5000: in pratica eseguo un servizio con l'immagine "registry" di DockerHub.

  ```shell
  ~> docker service create --name registry --publish 5000:5000 registry
  31vtmqlf3cl4z1dknibxqyh3x
  overall progress: 1 out of 1 tasks 
  1/1: running   [==================================================>] 
  verify: Service converged 
  ```

Esecuzione dell'applicativo sullo swarm. Tutti i comandi a seguire sono eseguiti in una cartella contenente i file del lato server precedentemente elencati:

- creazione dell'immagine:

  ```shell
  ~> docker-compose build
  Building server
  Step 1/13 : FROM debian
  latest: Pulling from library/debian
  e9afc4f90ab0: Pull complete
  Digest: sha256:46d659005ca1151087efa997f1039ae45a7bf7a2cbbe2d17d3dcbda632a3ee9a
  Status: Downloaded newer image for debian:latest
   ---> 1b686a95ddbf
  Step 2/13 : WORKDIR /root
   ---> Running in 71affde98c63
  Removing intermediate container 71affde98c63
   ---> c349dcd6b129
  Step 3/13 : COPY server.c ./
   ---> 0a2073782ff3
  Step 4/13 : COPY Makefile ./
   ---> 311cd6d021bb
  Step 5/13 : RUN DEBIAN_FRONTEND=noninteractive apt-get -y update
   ---> Running in e1b8b8a16280
  [output installazione pacchetti]
  Removing intermediate container 5fe69d3e59f6
   ---> d418113afaf3
  Step 8/13 : RUN DEBIAN_FRONTEND=noninteractive make;
   ---> Running in 405a6e486a2d
  gcc -c -ansi -Wall -Wno-comment -Wunused -pedantic -D_GNU_SOURCE server.c
  gcc  -o server.exe server.o -lpthread 
  Removing intermediate container 405a6e486a2d
   ---> 04927ec20e4d
  Step 9/13 : RUN DEBIAN_FRONTEND=noninteractive make cleanobj;
   ---> Running in b8d3b2417956
  rm -f server.o
  rm -f *~ core* a.out
  Removing intermediate container b8d3b2417956
   ---> 4c5b9b9d8b84
  Step 10/13 : RUN DEBIAN_FRONTEND=noninteractive apt-get -y purge gcc libc6-dev;
   ---> Running in 4fc2ad249ba2
  Reading package lists...
  Building dependency tree...
  Reading state information...
  The following packages were automatically installed and are no longer required:
    binutils binutils-common binutils-x86-64-linux-gnu cpp cpp-8 gcc-8 libasan5
    libatomic1 libbinutils libc-dev-bin libcc1-0 libgcc-8-dev libgomp1 libisl19
    libitm1 liblsan0 libmpc3 libmpfr6 libmpx2 libquadmath0 libtsan0 libubsan1
    linux-libc-dev manpages-dev
  Use 'apt autoremove' to remove them.
  The following packages will be REMOVED:
    gcc* libc6-dev*
  0 upgraded, 0 newly installed, 2 to remove and 0 not upgraded.
  After this operation, 19.4 MB disk space will be freed.
  (Reading database ... 12269 files and directories currently installed.)
  Removing gcc (4:8.3.0-1) ...
  Removing libc6-dev:amd64 (2.28-10) ...
  Removing intermediate container 4fc2ad249ba2
   ---> 5a9dcff534a8
  Step 11/13 : RUN DEBIAN_FRONTEND=noninteractive apt-get -y clean;
   ---> Running in 3571775256f5
  Removing intermediate container 3571775256f5
   ---> 4ae73787efe0
  Step 12/13 : EXPOSE 61000
   ---> Running in 8cf730b25ec0
  Removing intermediate container 8cf730b25ec0
   ---> ea2bdf301fc1
  Step 13/13 : CMD ./server.exe 61000 eth0 DOCKER_SWARM_NODE_ID
   ---> Running in ffd90b6da801
  Removing intermediate container ffd90b6da801
   ---> db49037bd862
  Successfully built db49037bd862
  Successfully tagged localhost:5000/server:latest
  
  ~> docker images
  REPOSITORY              TAG                 IMAGE ID            CREATED             SIZE
  localhost:5000/server   latest              db49037bd862        2 minutes ago       288MB
  debian                  latest              1b686a95ddbf        3 days ago          114MB
  registry                <none>              708bc6af7e5e        4 months ago        25.8MB
  ```

- upload dell'immagine nel repository dello swarm precedentemente deployato ==non sono riuscito con `docker-compose push`==:

  ```shell
  ~> docker-compose push
  Pushing server (localhost:5000/server:latest)
  The push refers to repository [localhost:5000/server]
  79a56bd99adf: Pushed
  052b27ffc38d: Pushed
  21f9e6461306: Pushed
  0d1b24878827: Pushed
  bcbd8986960f: Pushed
  16a21df5b73c: Pushed
  d391688b0459: Pushed
  091f211c1657: Pushed
  3df9c9c9767b: Pushed
  8803ef42039d: Pushed
  latest: digest: sha256:cca1aaf08c2ae2780fbbd34655e29997833db757aef2d0c5efba8c5930e3e088 size: 2410
  ```

- creo uno stack sullo swarm con il server:

  ```shell
  ~> docker stack deploy --compose-file docker-compose.yml applicativo
  Ignoring unsupported options: build
  
  Creating network applicativo_default
  Creating service applicativo_server
  
  ~> docker stack ls
  NAME                SERVICES            ORCHESTRATOR
  applicativo         1                   Swarm
  
  ~> docker stack services applicativo
  ID                  NAME                 MODE                REPLICAS            IMAGE                          PORTS
  jffeb5c8kzno        applicativo_server   replicated          1/1                 localhost:5000/server:latest   *:61000->61000/tcp
  ```

  Dall'output si può vedere che dopo aver messo in esecuzione il servizio descritto nel compose file nello swarm abbiamo in esecuzione uno stack con un unico servizio in esecuzione, di cui è presente solo una replica nello swarm, ovvero quella in esecuzione nel master.

==basta me so stufato, sarebbe da spiegare meglio nella teoria cosa sono i servizi e come li gestisce doccher, o cmq i sottogruppi dei comandi di docker: quali si dedicano allo swarm, quali ai container locali, etc==

#### Kubernetes

Kubernetes è un orchestratore di servizi su cluster di macchine.

Il modello di sviluppo di Kubernetes introduce regolarmente nuove funzionalità che tendono a rimpiazzare quelle già presenti, quindi le informazioni qui riportate potrebbero essere obsolete al momento della lettura.

##### Architettura

L'esecuzione dei servizi è distribuita su un cluster di **nodi**. Ogni nodo è un'istanza di ==Kubernetes daemon== e di norma una macchina (fisica o virtuale che sia) costituisce un nodo. Le specifiche di Kubernetes prevedono che i nodi del cluster siano connessi da una rete overlay criptata e che uno di loro svolga il ruolo di master. Il master, che si occupa di gestire i nodi, esegue vari servizi quali:

- API server: servizio che fornisce un'API di gestione del cluster;
- scheduler e controller: entità che dispiegano e gestiscono i servizi;
- etcd: database del cluster.

I container da eseguire sui nodi sono organizzati in **pod**. Un pod è uno spazio di esecuzione in cui i container eseguono come se fossero eseguiti in una stessa macchina fisica, condividendo alcune risorse, come uno stesso filesystem ed un indirizzo IP. Questo implica che:

- ci si può riferire a tutti i container di un pod tramite un unico indirizzo IP (diversamente da quanto accade negli stack di Docker Swarm);
- è possibile comunicare tra i container di un pod tramite canali di IPC (Inter Process Communications,  like SystemV semaphores or POSIX shared memory) o porte sull'indirizzo di localhost;
- le porte esposte da un container non possono essere esposte dagli altri container, e che riferendosi ad una porta sull'indirizzo IP del pod si sta comunicando con un preciso container.

Lo scopo dei pod è quello di fornire uno spazio ove dispiegare un insieme di servizi che nell'era pre-container sarebbero stati installati in una stessa macchina; per questo li chiama anche "logical host" (macchine logiche). Il valore aggiunto di un pod è la possibilità di distribuirne l'esecuzione di più pod su più nodi (quindi un nodo può eseguire più pod). Kubernetes inoltre permette di eseguire delle repliche di un pod con il quale effettuare il load-balancing delle richieste. L'insieme delle repliche di un pod è chiamato **deployment**.

Ogni deployment fornisce dei **servizi**, quindi in Kubernetes un servizio è un'astrazione verso l'utente che incapsula i criteri con il quale il servizio sarà accessibile dall'esterno (porte, permessi, etc.) e la logica di load-balancing utilizzata per suddividere il carico di lavoro tra i pod. I servizi possono essere sia pubblici che privati, ed è possibile definire servizi che utilizzano altri servizi. Nel caso di servizi dipendenti da altri servizi i servizi dipendenze saranno interrogati dai pod del servizio a dipendente.

![Architettura di Kubernetes.](images/Kubernetes - Architettura.png)

==L'amministrazione avviene attraverso il master del cluster. Kubernetes fornisce il client CLI "kubectl"==

##### Utilizzo

Per dispiegare pod e relativi servizi si utilizzano dei file di descrizione in formato YAML, come in Docker Swarm e Docker Compose.

Un caso semplice è rappresentato nella seguente figura, in cui si ipotizza che due pod denominati "sa-frontend" e "sa-frontend2" siano descritti dai file indicati sulla destra. Si noti come il nome di ciascun pod sia diverso, ma siano accomunati dalla stessa label "app: sa-frontend".

![Esempio.](images/Kubernetes - Esempio.png)

##### Esercitazione su Kubernetes

Questa esercitazione mostrerà l'utilizzo di Kubernetes. Il playground è quello in @fig:esercitazione_kubernetes_playground.

![Playrground dell'esercitazione.](images/Kubernetes - Esercitazione: playground.svg){#fig:esercitazione_kubernetes_playground}

Componenti del playground:

- host: macchina fisica, in questo caso istanza di Windows;
- cygwin: shell GNU per Windows;
- VirtualBox: hypervisor per l'esecuzione di Ubuntu e Minikube VM;
- Minikube VM: macchina virtuale, gestita dall'applicazione Minikube installata sull'host, nel quale sarà simulato un cluster di nodi. La macchina utilizza l'HV ma non è visibile all'utente a meno che lo richieda esplicitamente, visto che teoricamente non è necessario accedervi;
- Ubuntu: macchina virtuale dove sarà eseguito il client dell'applicazione dispiegata sul cluster Kubernetes, ovvero su Minikube;
- una cartella "/MINUKUBE" condivisa tra tutte le entità, dove risiederanno le applicazioni da eseguire e testare.

L'applicazione che vogliamo deployare è un guestbook. I servizi di cui sarà composta sono:

- redis-master: servizio che gestisce un database Redis;
- redis-slave: servizio che riceve ordini di lettura dal database Redis e li inoltra al componente redis master;
- web-server: servizio che fornisce un frontend web in PHP che mostra all'utente un'interfaccia attraverso il quale specificare il messaggio da inserire nel guestbook. Successivamente mostra tutte le frasi contenute nel database.

![Servizi dell'applicazione guestbook.](images/Kubernetes - Esercitazione: servizi guestbook.svg)

Per com'è composta si possono individuare subito i servizi replicabili: il redis slave ed il web server. Non è possibile replicare il redis master in quanto contiene il database, quindi se fosse replicato sarebbero create molteplici istanze del database del guestbook.

==Tutte le comunicazioni tra i servizi avvengono via rete e si individuano gli altri servizi tramite il loro nome DNS.==

###### Kubernetizzazione dell'applicazione

Per eseguire l'applicazione su Kubernetes, ovvero per "kubernetizzarla", si definiscono per ogni servizio un deployment e relativi service per ogni servizio dell'applicazione. Essendo web-server e redis-slave dei servizi replicabili i relativi deployment saranno composti da più pod, cosicché si possa bilanciare il carico di lavoro attraverso il service definito a monte. Il risultato è quello mostrato in @fig:esercitazione_kubernetes_applicazione_kubernetizzata.

![Struttura kubernetizzata dell'applicazione guestbook](images/Kubernetes - Esercitazione: guestbook kubernetizzato.svg)

Si noti che esistono delle dipendenze tra i service creati e che siano specificate più repliche soltanto dei pod relativi ai servizi web server e redis slave. La logica di bilanciamento del carico utilizzata sarà quella di default, non essendo state individuate particolari esigenze.

Di seguito si riportano i file relativi ai service e relativa analisi sommaria. I file `*-deployment.yaml` descrivono i deployment, mentre i file `*-service.yaml` descrivono i service dei relativi deployment. Tutti i file indicano la versione dell'API utilizzata, visto che tra macro-versioni non è mantenuta la retrocompatibilità (come per i file Docker Compose).

- servizio redis-master:

  - [redis-master-deployment.yaml](files/Kubernetes - Esempio guestbook/redis-master-deployment.yaml):

    ```yaml
    apiVersion: v1
    kind: Deployment  # tipo dell'entità da dispiegare
    [...]
    metadata:
      name: redis-master  # nome del deployment
      labels:
        app: redis
      [...]
    spec:
      replicas: 1  # soltanto un pod per il servizio redis-master
      [...]
      containers:
      - name: master  # i container di redis-master si chiameranno "master" BLEH
    	  image: redis  # immagine del container
    	  resources:  # risorse da riservare per questo container in ciascun nodo
    	    requests:  # risorse per le richieste
    	      cpu: 100m  # 100/millesimi, ovvero il 10% della CPU
    	      memory: 100Mi  # 100 MiB di RAM
    	  ports:
    	  - containerPort
    [...]
    ```

  - [redis-master-service.yaml](files/Kubernetes - Esempio guestbook/redis-master-service.yaml):

    ```yaml
    apiVersion: v1
    kind: Service
    metadata:
    	name: redis-master  # nome del servizio
    	[...]
    spec:
    	ports:
        - port: 6379
          targetPort: 6379
        selector:  # selettore che indica i pod da utilizzare per questo servizio
        
    ```

    Il nome del servizio deve essere corretto, perché il DNS interno a Kubernetes fornirà l'IP di questo service ai pod che richiederanno di comunicare con questo servizio.

- servizio redis-slave:

  - [redis-slave-deployment.yaml](files/Kubernetes - Esempio guestbook/redis-slave-deployment.yaml):

    ```yaml
    [...]
      replicas: 2  # sto giro dispieghiamo 2 pod
      [...]
        containers:
        - name: slave
          env:
          - name: GET_HOSTS_FROM
            value: dns
          ports:
          - containerPort: 6379
    	  resources:  # risorse da riservare per questo container in ciascun nodo
    	    requests:  # risorse per le richieste
    	      cpu: 100m  # 100/millesimi, ovvero il 10% della CPU
    	      memory: 100Mi  # 100 MiB di RAM
    [...]
    ```

    La variabile d'ambiente dei container del pod redis-slave "GET_HOSTS_FROM" indica al programma che eseguono di utlizzare il servizio DNS per trovare l'IP del servizio redis-master. L'utilizzo della variabile di ambiente permette nel caso si utilizzi qualcosa al posto di Kubernetes che non fornisce un servizio DNS di impostare manualmente dove il programma può trovare tali indirizzi IP.

  - [redis-slave-service.yaml](files/Kubernetes - Esempio guestbook/redis-slave-service.yaml)

- servizio frontend:

  - [frontend-deployment.yaml](files/Kubernetes - Esempio guestbook/frontend-deployment.yaml)
  
  - [frontend-service.yaml](files/Kubernetes - Esempio guestbook/frontend-service.yaml):
  
    ```yaml
    [...]
    spec:
      type: NodePort
      ports:
      - port: 80
    [...]
    ```
  
    `type: NodePort` indica a Minikube di pubblicare questa porta anche sul cluster, non solo sul pod, in modo che sia raggiungibile dall'esterno. Questo parametro è supportato solo da Minikube, in altri sistemi con Kubernetes potrebbero essere utilizzati approcci differenti.

###### Esecuzione della MinikubeVM

```shell
host:/> minukube start
[...]
"minikube" IP address is 192.168.99.100
Configuring Docker as the container runtime ...
[...]
kubectl is now configured to use "minikube"
[...]
```

Minikube utilizzerà uno degli hypervisor installati e supportati. In questo caso utilizzerà Virtualbox in modalità detached headless, quindi non sarà visibile niente all'utente; la MinikubeVM sarà comunque accedibile o all'indirizzo su interfaccia virtuale interna 192.168.99.100 ==tramite SSH== o tramite l'interfaccia di Virtualbox, che se avviata mostrerà una voce per tale VM

Una volta acceduti sarà mostrata la sua shell (non è richiesto login). La MinikubeVM è basata su un sistema operativo molto minimale che fornisce le funzionalità strettamente necessarie allo scopo per il quale è sviluppata, quindi i comandi disponibili non sono molti. Tra i tool forniti troviamo:

- Docker: utilizzabile con il comando `docker`;
- Kubernetes: 
  - è possibile anche avviare la dashboard eseguendo il comando `minikube dashboard` direttamente dall'host;

###### Dispiegamento dei servizi Kubernetes

1.  Deploy del deployment di redis-master:

    ```shell
    host:/> kubectl apply -f redis-master-deployment.yaml
    deployment.apps/redis-master created
    ```

    Stato dei pod dispiegati:

    ```shell
    host:/> kubectl get pods
    NAME                          READY  STATUS   RESTARTS  AGE
    redis-master-596696dd4-6sshm  1/1    Running  0         16s
    ```

    - READY 1/1: soltanto un pod è stato dispiegato per questo deployment e quell'unico pod lavora bene;
    - RESTARTS: Kubernetes cerca di riavviare automaticamente un pod, ma per ora non è mai successo.

    Deploy del service a monte del deployment:

    ```shell
    host:/> kubectl apply -f redis-master-service.yaml
    service/redis-master created
    ```

    Stato dei servizi dispiegati:

    ```shell
    host:/> kubectl get service
    NAME          TYPE       CLUSTER-IP      EXTERNAL-IP  PORT(S)   AGE
    kubernetes    ClusterIP  10.96.0.1       <none>       443/TCP   3h17m
    redis-master  ClusterIP  10.104.227.161  <none>       6397/TCP  10s
    ```

    - CLUSTER-IP: indica l'IP del service all'interno della rete di overlay creata da Kubernetes;

    Il servizio Kubernetes è presente di default.

2.  Deploy del deployment di redis-slave:

    ```shell
    host:/> kubectl apply -f redis-slave-deployment
    deployment.apps/redis-slave created
    ```

    Stato dei pod dispiegati:

    ```shell
    host:/> kubectl get pods
    NAME                          READY  STATUS   RESTARTS  AGE
    redis-master-596696dd4-6sshm  1/1    Running  0         27m
    redis-slave-96685cfdb-hdmh5   1/1	 Running  0         6s
    redis-slave-96685cfdb-kzsrv   1/1	 Running  0         6s
    ```

    Deploy del service a monte del deployment:

    ```shell
    host:/> kubectl apply -f redis-slave-service.yaml
    service/redis-slave created
    ```

    Stato dei servizi dispiegati:

    ```shell
    host:/> kubectl get service
    NAME          TYPE       CLUSTER-IP      EXTERNAL-IP  PORT(S)   AGE
    kubernetes    ClusterIP  10.96.0.1       <none>       443/TCP   3h25m
    redis-master  ClusterIP  10.104.227.161  <none>       6397/TCP  8m41s
    redis-slave   ClusterIP  10.99.94.157    <none>       6397/TCP  8s
    ```

    Deploy del service

3. Deploy del deployment di frontend

   [...]

   ```shell
   host:/> kubectl get pods
   NAME                          READY  STATUS   RESTARTS  AGE
   frontend-69859f6796-7hpbm     1/1    Running  0         4s
   frontend-69859f6796-7wzxs     1/1    Running  0         4s
   frontend-69859f6796-w8ggs     1/1    Running  0         4s
   redis-master-596696dd4-6sshm  1/1    Running  0         31m
   redis-slave-96685cfdb-hdmh5   1/1	 Running  0         4m37s
   redis-slave-96685cfdb-kzsrv   1/1	 Running  0         4m37s
   ```

   ```shell
   host:/> kubectl get service
   NAME          TYPE       CLUSTER-IP      EXTERNAL-IP  PORT(S)       AGE
   frontend      NodePort   10.109.31.75    <none>       80:30424/TCP  4s
   kubernetes    ClusterIP  10.96.0.1       <none>       443/TCP       3h30m
   redis-master  ClusterIP  10.104.227.161  <none>       6397/TCP      13m
   redis-slave   ClusterIP  10.99.94.157    <none>       6397/TCP      5m15s
   ```

   `80:30424/TCP`: la porta 30424 del deployment è stata mappata sulla porta 80.

4.  Ottengo l'URL dell'applicazione:

    ```shell
    host:/> minikube service frontend --url
    http://192.168.99.100:80
    ```

    Nel caso la porta 80 fosse in conflitto con un'altra nell'host fisico Kubernetes ne utilizzerà un'altra.

    La stessa informazione sarebbe visibile dall'output di `kubectl get service frontend`.

###### Aumento delle risorse destinate all'applicazione

Aumento delle repliche per il servizio frontend:

```shell
host:/> kubectl scale deployment frontend --replicas=5
```

Diminuzione delle repliche per il servizio redis-slave:

```shell
host:/> kubectl scale deployment redis-slave --replicas=1
```

###### Eliminazione dell'applicazione

Due possibilità:

- eliminazione secca del cluster:

  ```shell
  host:/> minikube ???
  ```

- shutdown a passi:

  ```shell
  host:/> kubectl delete deployment -i app=redis
  host:/> kubectl delete service -i app=redis
  host:/> kubectl delete deployment -i app=guestbook
  host:/> kubectl delete service -i app=guestbook
  host:/> minikube stop  # Chiude la MinikubeVM salvandone lo stato
  ```

### Container Microsoft

Microsoft fornisce due tecnologie di containerization:

- container Hyper-V: unità di isolamento che, a conti fatti, è una VM. Attenzione che si comporta in modo diverso sulle versioni desktop e server di Windows;
- container di Windows Server: effettivamente un container.
  - esiste una versione headless di Windows Server, Windows Nano Server, apposita per l'esecuzione di container e/o altri servizi senza pesare come la versione full. Solo x64, no GUI, attivazione automatica senza PKEY e varie altre funzionalità che potrebbero essere pesanti disabilitate.
