# Casi di applicazione

## Integrazione col mondo dell'automazione

Le macchine automatiche sono controllate da PLC (Programmable Logic Controllers). Con l'avvento dell'industria 4.0 spesso occorre esportare in realtime i dati che tali PLC elaborano per scopi di più ampio spettro (data analysis, intelligenza arificiale, ...).