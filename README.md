Corso tenuto dal prof. [Vittorio Ghini](https://www.unibo.it/sitoweb/vittorio.ghini).

Il corso si occupa di progettazione, dispiegamento e manutenzione di sistemi software tipicamente multi-utente, distribuiti e virtualizzati.

Ambiti di cui si occuperà il corso:

- mondo dell'automazione;
- applicazioni monolitiche vs. sistemi a micro-servizi;
- sistemi a micro-servizi in cloud;
- sicurezza di sistemi informatici;
- IAM (Identity and Access Management), quindi Directory Service;
- deployment scalabile mediante virtualizzazione e container;
- scenari di rete.

## Conversione degli appunti in PDF

Utilizzando [pandoc](https://www.pandoc.org/) occorre semplicemente eseguire il seguente comando:

```
pandoc -d pdf_defaults.yaml
```

